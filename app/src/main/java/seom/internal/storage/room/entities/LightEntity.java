package seom.internal.storage.room.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class LightEntity implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String datetime;
    private double lx;

    public int getId() {
        return id;
    }

    // luminous flux per unit area. measure of light intensity.
    public double getLx() {
        return lx;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    // luminous flux per unit area. measure of light intensity.
    public void setLx(double lx) {
        this.lx = lx;
    }

    public LightEntity setGetId(int id) {
        this.id = id;
        return this;
    }

    public LightEntity setGetDatetime(String datetime) {
        this.datetime = datetime;
        return this;
    }

    public LightEntity setGetLX(double lx) {
        this.lx = lx;
        return this;
    }

    public String toString(){
        return "LightEntity("+getDatetime()+", "+ getLx()+")";
    }
}
