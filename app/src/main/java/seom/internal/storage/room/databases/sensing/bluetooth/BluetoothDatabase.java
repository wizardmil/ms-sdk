package seom.internal.storage.room.databases.sensing.bluetooth;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import seom.internal.storage.room.entities.BluetoothEntity;

@Database(entities = {BluetoothEntity.class}, version = 1, exportSchema = false)
public abstract class BluetoothDatabase extends RoomDatabase {

    public abstract BluetoothDaoAccess daoAccess();
}
