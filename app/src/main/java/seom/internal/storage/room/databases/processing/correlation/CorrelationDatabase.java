package seom.internal.storage.room.databases.processing.correlation;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import seom.internal.storage.room.entities.CorrelationEntity;

@Database(entities = {CorrelationEntity.class}, version = 1, exportSchema = false)
public abstract class CorrelationDatabase extends RoomDatabase {

    public abstract CorrelationDaoAccess daoAccess();
}
