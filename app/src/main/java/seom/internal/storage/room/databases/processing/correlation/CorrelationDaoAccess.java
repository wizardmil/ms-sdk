package seom.internal.storage.room.databases.processing.correlation;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import seom.internal.storage.room.entities.CorrelationEntity;

@Dao
public interface CorrelationDaoAccess {
    @Insert
    Long inserTask(CorrelationEntity entity);

    @Query("SELECT * FROM CorrelationEntity ORDER BY datetime")
    List<CorrelationEntity> fetchAllEntities();

    @Query("SELECT * FROM CorrelationEntity ORDER BY datetime DESC LIMIT :size")
    List<CorrelationEntity> fetchNumberOfEntities(int size);

    @Update
    void updateTask(CorrelationEntity entity);

    @Delete
    void deleteTask(CorrelationEntity entity);

    @Query("SELECT * FROM CorrelationEntity WHERE id =:taskId")
    LiveData<CorrelationEntity> getTask(int taskId);

    @Query("SELECT COUNT(id) FROM CorrelationEntity")
    Integer getRowCount();

    @Query("DELETE FROM CorrelationEntity")
    void purgeTable();

    @Query("DELETE FROM CorrelationEntity WHERE id NOT IN ( :lst )")
    void purgeAfterOffset(String lst);

    @Query("SELECT id FROM CorrelationEntity ORDER BY datetime DESC LIMIT :n")
    List<Integer> getIds(int n);
}
