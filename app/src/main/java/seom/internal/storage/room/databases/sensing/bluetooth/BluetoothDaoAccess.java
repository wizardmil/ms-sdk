package seom.internal.storage.room.databases.sensing.bluetooth;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import seom.internal.storage.room.entities.BluetoothEntity;

@Dao
public interface BluetoothDaoAccess {

    @Insert
    Long inserTask(BluetoothEntity entity);

    @Query("SELECT * FROM BluetoothEntity ORDER BY datetime")
    List<BluetoothEntity> fetchAllBluetoothEntity();

    @Query("SELECT * FROM BluetoothEntity ORDER BY datetime DESC LIMIT :size")
    List<BluetoothEntity> fetchNumberOfEntities(int size);

    @Update
    void updateTask(BluetoothEntity entity);

    @Delete
    void deleteTask(BluetoothEntity entity);

    @Query("SELECT * FROM BluetoothEntity WHERE id =:taskId")
    LiveData<BluetoothEntity> getTask(int taskId);

    @Query("SELECT COUNT(id) FROM BluetoothEntity")
    LiveData<Integer> getRowCount();

    @Query("DELETE FROM BluetoothEntity")
    void purgeTable();
}
