package seom.internal.storage.room.databases.sensing.light;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import seom.internal.storage.room.entities.LightEntity;

@Dao
public interface LightDaoAccess {

    @Insert
    Long inserTask(LightEntity entity);

    @Query("SELECT * FROM LightEntity ORDER BY datetime")
    LiveData<List<LightEntity>> fetchAllLocationEntities();

    @Update
    void updateTask(LightEntity entity);

    @Delete
    void deleteTask(LightEntity entity);

    @Query("SELECT * FROM LightEntity WHERE id =:taskId")
    LiveData<LightEntity> getTask(int taskId);

    @Query("SELECT COUNT(id) FROM LightEntity")
    LiveData<Integer> getRowCount();
}