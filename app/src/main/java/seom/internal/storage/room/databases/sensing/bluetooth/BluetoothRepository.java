package seom.internal.storage.room.databases.sensing.bluetooth;

import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Room;
import seom.controller.CONFIG;
import seom.internal.storage.room.entities.BluetoothEntity;

public class BluetoothRepository {

    private final String DB_NAME = "SENSING_BLUETOOTH";

    private static BluetoothDatabase bluetoothDatabase;

    private static BluetoothRepository instance;

    public LiveData<Integer> rowCount;

    public BluetoothRepository(){
        bluetoothDatabase = Room.databaseBuilder(CONFIG.CONTEXT, BluetoothDatabase.class, DB_NAME).build();
    }

    public static BluetoothRepository getInstance(){
        if (instance == null){
            instance = new BluetoothRepository();
        }
        return instance;
    }

    public void insertTask(String datetime, String name, String MACAdress) {

        BluetoothEntity node = new BluetoothEntity();
        node.setDatetime(datetime);
        node.setGetName(name);
        node.setMACAdress(MACAdress);

        insertTask(node);
    }

    public LiveData<Integer> getCount() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                BluetoothRepository.getInstance().rowCount = bluetoothDatabase.daoAccess().getRowCount();
                return null;
            }

        }.execute();

        return BluetoothRepository.getInstance().rowCount;
    }

    public void insertTask(final BluetoothEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                bluetoothDatabase.daoAccess().inserTask(node);
                return null;
            }

        }.execute();
    }

    public void updateTask(final BluetoothEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                bluetoothDatabase.daoAccess().updateTask(node);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final int id) {
        final LiveData<BluetoothEntity> task = getTask(id);
        if(task != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    bluetoothDatabase.daoAccess().deleteTask(task.getValue());
                    return null;
                }
            }.execute();
        }
    }

    public void deleteTask(final BluetoothEntity note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                bluetoothDatabase.daoAccess().deleteTask(note);
                return null;
            }
        }.execute();
    }

    public LiveData<BluetoothEntity> getTask(int id) {
        return bluetoothDatabase.daoAccess().getTask(id);
    }

    public List<BluetoothEntity> getTasks() {
        return bluetoothDatabase.daoAccess().fetchAllBluetoothEntity();
    }

    public List<BluetoothEntity> getNumberOfTasks(int size) {
        return bluetoothDatabase.daoAccess().fetchNumberOfEntities(size);
    }

    /**
     * IMPORTANT! MUST NOT BE CALLED ON UI THREAD
     */
    public void enableTotalTableMeltdown(){
        bluetoothDatabase.daoAccess().purgeTable();
    }
}
