package seom.internal.storage.room.databases.processing;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import seom.internal.storage.room.entities.ClusterEntity;

@Database(entities = {ClusterEntity.class}, version = 1, exportSchema = false)
public abstract class ProcessedLocDatabase extends RoomDatabase {

    public abstract ProcessedLocDaoAccess daoAccess();
}
