package seom.internal.storage.room.databases.sensing.location;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.util.List;

import seom.controller.CONFIG;
import seom.internal.storage.room.entities.LocationEntity;

public class LocationRepository {

    private final String DB_NAME = "SENSING_LOCATION";

    private static LocationDatabase locationDatabase;

    private static LocationRepository instance;

    public LiveData<Integer> rowCount;

    public LocationRepository(){
        locationDatabase = Room.databaseBuilder(CONFIG.CONTEXT, LocationDatabase.class, DB_NAME).build();
    }

    public static LocationRepository getInstance(){
        if (instance == null){
            instance = new LocationRepository();
        }
        return instance;
    }

    public void insertTask(String datetime, double latitude, double longitude) {

        LocationEntity node = new LocationEntity();
        node.setDatetime(datetime);
        node.setLatitute(latitude);
        node.setLongitute(longitude);

        insertTask(node);
    }

    public LiveData<Integer> getCount() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                LocationRepository.getInstance().rowCount = locationDatabase.daoAccess().getRowCount();
                return null;
            }

        }.execute();

        return LocationRepository.getInstance().rowCount;
    }

    public void insertTask(final LocationEntity node) {

       new AsyncTask<Void, Void, Void>() {
           @Override
           protected Void doInBackground(Void... voids){
               locationDatabase.daoAccess().inserTask(node);
                return null;
           }

       }.execute();
    }

    public void updateTask(final LocationEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                locationDatabase.daoAccess().updateTask(node);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final int id) {
        final LiveData<LocationEntity> task = getTask(id);
        if(task != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    locationDatabase.daoAccess().deleteTask(task.getValue());
                    return null;
                }
            }.execute();
        }
    }

    public void deleteTask(final LocationEntity note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                locationDatabase.daoAccess().deleteTask(note);
                return null;
            }
        }.execute();
    }

    public LiveData<LocationEntity> getTask(int id) {
        return locationDatabase.daoAccess().getTask(id);
    }

    public List<LocationEntity> getNumberOfTasks(int size) {
        return locationDatabase.daoAccess().fetchAllLocationEntities(size);
    }

    public List<LocationEntity> getAllTasks() {
        return locationDatabase.daoAccess().fetchAllEntities();
    }

    /**
     * IMPORTANT! MUST NOT BE CALLED ON UI THREAD
     */
    public void enableTotalTableMeltdown(){
        locationDatabase.daoAccess().purgeTable();
    }
}
