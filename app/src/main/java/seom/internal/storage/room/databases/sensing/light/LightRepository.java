package seom.internal.storage.room.databases.sensing.light;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.util.List;

import seom.controller.CONFIG;
import seom.internal.storage.room.databases.sensing.location.LocationRepository;
import seom.internal.storage.room.entities.LightEntity;

public class LightRepository {
    private final String DB_NAME = "LIGHT";

    private static LightDatabase lightDatabase;

    private static LightRepository instance;

    public LiveData<Integer> rowCount;

    public LightRepository(){
        lightDatabase = Room.databaseBuilder(CONFIG.CONTEXT, LightDatabase.class, DB_NAME).build();
    }

    public static LightRepository getInstance(){
        if (instance == null){
            instance = new LightRepository();
        }
        return instance;
    }

    public static void insertTask(String datetime, double lx) {

        LightEntity node = new LightEntity();
        node.setDatetime(datetime);
        node.setLx(lx);

        insertTask(node);
    }

    public static LiveData<Integer> getCount() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                LocationRepository.getInstance().rowCount = lightDatabase.daoAccess().getRowCount();
                return null;
            }

        }.execute();

        return LocationRepository.getInstance().rowCount;
    }

    public static void insertTask(final LightEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                lightDatabase.daoAccess().inserTask(node);
                return null;
            }

        }.execute();
    }

    public static void updateTask(final LightEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                lightDatabase.daoAccess().updateTask(node);
                return null;
            }
        }.execute();
    }

    public static void deleteTask(final int id) {
        final LiveData<LightEntity> task = getTask(id);
        if(task != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    lightDatabase.daoAccess().deleteTask(task.getValue());
                    return null;
                }
            }.execute();
        }
    }

    public static void deleteTask(final LightEntity note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                lightDatabase.daoAccess().deleteTask(note);
                return null;
            }
        }.execute();
    }

    public static LiveData<LightEntity> getTask(int id) {
        return lightDatabase.daoAccess().getTask(id);
    }

    public static LiveData<List<LightEntity>> getTasks() {
        return lightDatabase.daoAccess().fetchAllLocationEntities();
    }
}
