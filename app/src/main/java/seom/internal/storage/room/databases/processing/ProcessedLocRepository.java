package seom.internal.storage.room.databases.processing;

import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import seom.controller.CONFIG;
import seom.internal.storage.room.databases.sensing.location.LocationRepository;
import seom.internal.storage.room.entities.ClusterEntity;
import seom.objects.ProcessingObject;

public class ProcessedLocRepository {

    private final String DB_NAME = "PROCESSED_LOCATION";

    private static ProcessedLocDatabase locationDatabase;

    private static ProcessedLocRepository instance;

    public LiveData<Integer> rowCount;

    public ProcessedLocRepository(){
        super();
        locationDatabase = Room.databaseBuilder(CONFIG.CONTEXT, ProcessedLocDatabase.class, DB_NAME).build();
    }

    public static ProcessedLocRepository getInstance(){
        if (instance == null){
            instance = new ProcessedLocRepository();
        }
        return instance;
    }

    public void insertTask(String place, String addressLine, String locality, String adminArea, String countryName, String postalCode, String featureName, String datetime, String startTime, String endTime) {

        ClusterEntity node = new ClusterEntity();
        node.setAddressLine(addressLine);
        node.setLocality(locality);
        node.setAdminArea(adminArea);
        node.setCountryName(countryName);
        node.setPostalCode(postalCode);
        node.setFeatureName(featureName);
        node.setDatetime(datetime);
        node.setStartTime(startTime);
        node.setEndTime(endTime);
        node.setBelonging(place);
        insertTask(node);
    }

    public void insertProcessingObject(ProcessingObject processingObject){
        ArrayList<ClusterEntity> obj =  formatProcessingObject(processingObject);

        for (int i = 0; i < obj.size(); i++){
            ProcessedLocRepository.getInstance().insertTask(obj.get(i));
            Log.d("DEBUGGING-PROCESSING", "stored processing object "+ obj.get(i).toString());
        }
    }

    public ArrayList<ClusterEntity> formatProcessingObject(ProcessingObject processingObject){
        ArrayList<ClusterEntity> entities = new ArrayList<>();

        for (int i = 0; i < processingObject.getBatchSize(); i++){
            HashMap<String, ArrayList<String>> currentMap = (HashMap<String, ArrayList<String>>)processingObject.getData().get(i);
            for (String key : currentMap.keySet()){
                entities.add(
                        new ClusterEntity()
                                .setGetBelonging(key)
                                .setGetAddressLine(currentMap.get(key).get(0))
                                .setGetLocality(currentMap.get(key).get(1))
                                .setGetAdminArea(currentMap.get(key).get(2))
                                .setGetCountryName(currentMap.get(key).get(3))
                                .setGetPostalCode(currentMap.get(key).get(4))
                                .setGetFeatureName(currentMap.get(key).get(5))
                                .setGetStartTime(currentMap.get(key).get(6))
                                .setGetEndTime(currentMap.get(key).get(7))
                                .setGetDatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()))
                );
            }
        }

        return entities;
    }

    public LiveData<Integer> getCount() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                LocationRepository.getInstance().rowCount = locationDatabase.daoAccess().getRowCount();
                return null;
            }

        }.execute();

        return LocationRepository.getInstance().rowCount;
    }

    public void insertTask(final ClusterEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                locationDatabase.daoAccess().inserTask(node);
                return null;
            }

        }.execute();
    }

    public void updateTask(final ClusterEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                locationDatabase.daoAccess().updateTask(node);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final int id) {
        final LiveData<ClusterEntity> task = getTask(id);
        if(task != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    locationDatabase.daoAccess().deleteTask(task.getValue());
                    return null;
                }
            }.execute();
        }
    }

    public void deleteTask(final ClusterEntity note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                locationDatabase.daoAccess().deleteTask(note);
                return null;
            }
        }.execute();
    }

    public LiveData<ClusterEntity> getTask(int id) {
        return locationDatabase.daoAccess().getTask(id);
    }

    public List<ClusterEntity> getTasks() {
        return locationDatabase.daoAccess().fetchAllEntities();
    }

    public List<ClusterEntity> getNumberOfTasks(int size){

        return locationDatabase.daoAccess().fetchNumberOfEntities(size);
    }

    /**
     * IMPORTANT! MUST NOT BE CALLED ON UI THREAD
     */
    public void enableTotalTableMeltdown(){
        locationDatabase.daoAccess().purgeTable();
    }
}
