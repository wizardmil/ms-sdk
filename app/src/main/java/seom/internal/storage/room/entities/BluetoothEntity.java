package seom.internal.storage.room.entities;

import java.io.Serializable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class BluetoothEntity implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String datetime;
    private String name;
    private String MACAdress;

    public int getId() {
        return id;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getName() {
        return name;
    }

    public String getMACAdress() {
        return MACAdress;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMACAdress(String MACAdress) {
        this.MACAdress = MACAdress;
    }

    public String getCompareableString(){
        return (getName()+", "+getMACAdress()).replace(" ", "");
    }


    public BluetoothEntity setGetId(int id) {
        this.id = id;
        return this;
    }

    public BluetoothEntity setGetDatetime(String datetime) {
        this.datetime = datetime;
        return this;
    }

    public BluetoothEntity setGetName(String name) {
        this.name = name;
        return this;
    }


    public BluetoothEntity setGetMACAdress(String MACAdress) {
        this.MACAdress = MACAdress;
        return this;
    }

    public String toString(){
        return "BluetoothEntity("+getDatetime()+", "+getName()+", "+getMACAdress()+")";
    }
}
