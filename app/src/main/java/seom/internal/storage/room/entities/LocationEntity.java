package seom.internal.storage.room.entities;

import android.util.Pair;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class LocationEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String datetime;
    private double latitute;
    private double longitute;

    public int getId() {
        return id;
    }

    public double getLongitute() {
        return longitute;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setLatitute(double latitute) {
        this.latitute = latitute;
    }

    public void setLongitute(double longitute) {
        this.longitute = longitute;
    }

    public Pair<String, String> getAsPair(){
        return new Pair<>(Double.toString(latitute)+","+Double.toString(longitute),datetime);
    }

    public LocationEntity setGetId(int id) {
        this.id = id;
        return this;
    }

    public LocationEntity setGetDatetime(String datetime) {
        this.datetime = datetime;
        return this;
    }

    public double getLatitute() {
        return latitute;
    }

    public LocationEntity setGetLatitute(double latitute) {
        this.latitute = latitute;
        return this;
    }


    public LocationEntity setGetLongitute(double longitute) {
        this.longitute = longitute;
        return this;
    }

    public String toString(){
        return "LocationEntity("+getDatetime()+", "+getLatitute()+", "+getLongitute()+")";
    }
}
