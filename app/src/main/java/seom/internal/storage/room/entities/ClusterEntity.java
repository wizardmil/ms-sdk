package seom.internal.storage.room.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Entity
public class ClusterEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String belonging;
    private String addressLine;
    private String locality;
    private String adminArea;
    private String countryName;
    private String postalCode;
    private String featureName;
    private String startTime;
    private String endTime;
    private String datetime;

    public static ClusterEntity parseFromString(String str){
        List<String> data = Arrays.asList(str.replace("ClusterEntity", "").replace("(", "").replace(")", "").split(","));
        return new ClusterEntity().setGetBelonging(data.get(0)).setGetAddressLine(data.get(1)).setGetLocality(data.get(2)).setGetAdminArea(data.get(3)).setGetCountryName(data.get(4)).setGetPostalCode(data.get(5)).setGetFeatureName(data.get(6)).setGetStartTime(data.get(7)).setGetEndTime(data.get(8)).setGetDatetime(data.get(9));
    }

    public String toComparableString(){
        return getBelonging()+","+getAddressLine()+","+getLocality()+","+getAdminArea()+","+getCountryName()+","+getPostalCode()+","+getFeatureName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBelonging(){
        return this.belonging;
    }

    public void setBelonging(String belonging){
        this.belonging = belonging;
    }

    public ClusterEntity setGetBelonging(String belonging){
        this.belonging = belonging;
        return this;
    }

    public void setDatetime(String datetime){
        this.datetime = datetime;
    }

    public String getDatetime(){
        return this.datetime;
    }

    public ClusterEntity setGetDatetime(String datetime){
        this.datetime = datetime;
        return this;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public void setAdminArea(String adminArea) {
        this.adminArea = adminArea;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public void setStartTime(String time) { this.startTime = time; }

    public void setEndTime(String time) { this.endTime = time; }

    public String getStartTime() {return this.startTime;}

    public String getEndTime(){return this.endTime;}

    public ClusterEntity setGetStartTime(String time){
        this.startTime = time;
        return this;
    }

    public ClusterEntity setGetEndTime(String time){
        this.endTime = time;
        return this;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public ClusterEntity setGetAddressLine(String addressLine) {
        this.addressLine = addressLine;
        return this;
    }

    public String getLocality() {
        return locality;
    }

    public ClusterEntity setGetLocality(String locality) {
        this.locality = locality;
        return this;
    }

    public String getAdminArea() {
        return adminArea;
    }

    public ClusterEntity setGetAdminArea(String adminArea) {
        this.adminArea = adminArea;
        return this;
    }

    public String getCountryName() {
        return countryName;
    }

    public ClusterEntity setGetCountryName(String countryName) {
        this.countryName = countryName;
        return this;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public ClusterEntity setGetPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public String getFeatureName() {
        return featureName;
    }

    public ClusterEntity setGetFeatureName(String featureName) {
        this.featureName = featureName;
        return this;
    }

    @Override
    public String toString(){
        return "ClusterEntity("+getBelonging()+","+getAddressLine()+","+getLocality()+","+getAdminArea()+","+getCountryName()+","+getPostalCode()+","+getFeatureName()+","+getStartTime()+","+getEndTime()+","+getDatetime()+")";
    }
}
