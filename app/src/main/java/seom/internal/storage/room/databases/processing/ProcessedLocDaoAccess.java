package seom.internal.storage.room.databases.processing;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import seom.internal.storage.room.entities.ClusterEntity;

@Dao
public interface ProcessedLocDaoAccess {
    @Insert
    Long inserTask(ClusterEntity entity);

    @Query("SELECT * FROM ClusterEntity ORDER BY datetime")
    List<ClusterEntity> fetchAllEntities();

    @Query("SELECT * FROM ClusterEntity ORDER BY datetime DESC LIMIT :size")
    List<ClusterEntity> fetchNumberOfEntities(int size);

    @Update
    void updateTask(ClusterEntity entity);

    @Delete
    void deleteTask(ClusterEntity entity);

    @Query("SELECT * FROM ClusterEntity WHERE id =:taskId")
    LiveData<ClusterEntity> getTask(int taskId);

    @Query("SELECT COUNT(id) FROM ClusterEntity")
    LiveData<Integer> getRowCount();

    @Query("DELETE FROM ClusterEntity")
    void purgeTable();

}
