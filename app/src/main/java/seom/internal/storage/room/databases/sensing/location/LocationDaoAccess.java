package seom.internal.storage.room.databases.sensing.location;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import seom.internal.storage.room.entities.LocationEntity;

@Dao
public interface LocationDaoAccess {

    @Insert
    Long inserTask(LocationEntity entity);

    @Query("SELECT * FROM LocationEntity ORDER BY datetime DESC LIMIT :amount")
    List<LocationEntity> fetchAllLocationEntities(int amount);

    @Query("SELECT * FROM LocationEntity ORDER BY datetime")
    List<LocationEntity> fetchAllEntities();

    @Update
    void updateTask(LocationEntity entity);

    @Delete
    void deleteTask(LocationEntity entity);

    @Query("SELECT * FROM LocationEntity WHERE id =:taskId")
    LiveData<LocationEntity> getTask(int taskId);

    @Query("SELECT COUNT(id) FROM LocationEntity")
    LiveData<Integer> getRowCount();

    @Query("DELETE FROM LocationEntity")
    void purgeTable();
}