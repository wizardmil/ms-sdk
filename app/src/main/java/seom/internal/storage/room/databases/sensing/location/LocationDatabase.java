package seom.internal.storage.room.databases.sensing.location;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import seom.internal.storage.room.entities.LocationEntity;

@Database(entities = {LocationEntity.class}, version = 1, exportSchema = false)
public abstract class LocationDatabase extends RoomDatabase {

    public abstract LocationDaoAccess daoAccess();
}
