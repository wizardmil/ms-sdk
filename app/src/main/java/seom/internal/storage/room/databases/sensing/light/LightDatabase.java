package seom.internal.storage.room.databases.sensing.light;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import seom.internal.storage.room.entities.LightEntity;

@Database(entities = {LightEntity.class}, version = 1, exportSchema = false)
public abstract class LightDatabase extends RoomDatabase {

    public abstract LightDaoAccess daoAccess();
}
