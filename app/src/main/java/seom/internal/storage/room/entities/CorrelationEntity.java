package seom.internal.storage.room.entities;

import android.util.Log;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
public class CorrelationEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String datetime;
    private String cluster;
    private String bluetoothEntity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(ClusterEntity cluster){
        this.cluster = cluster.toComparableString();
    }

    public void setCluster(String cluster) { this.cluster = cluster; }

    public void updateBlueToothEntities(List<BluetoothEntity> bluetoothEntities){
        for(BluetoothEntity b : bluetoothEntities){
            if (!Arrays.asList(bluetoothEntity.split(";")).contains(b.toString())){
                bluetoothEntity += b.toString() + ";";
            }
        }
    }

    public List<String> getComparableBluetoothEntities(){
        List<String> devices = Arrays.asList(bluetoothEntity.replace("BluetoothEntity(", "").replace(")", "").split(";"));
        List<String> ret = new ArrayList<>();
        for(String s : devices){
            String[] sp = s.replace(" ", "").split(",");
            ret.add(sp[1]+","+sp[2]);
        }
        return ret;
    }

    public String getBluetoothEntity() {
        return bluetoothEntity;
    }

    public void setBluetoothEntity(List<BluetoothEntity> bluetoothEntity) {
        String converted = "";
        for(BluetoothEntity b : bluetoothEntity){
            converted += b.toString()+";";
        }
        this.bluetoothEntity = converted;
    }

    public void setBluetoothEntity(String bluetoothEntity) { this.bluetoothEntity = bluetoothEntity; }

    public CorrelationEntity setGetId(int id) {
        this.id = id;
        return this;
    }

    public CorrelationEntity setGetDatetime(String datetime) {
        this.datetime = datetime;
        return this;
    }

    public CorrelationEntity setGetBluetoothEntity(List<BluetoothEntity> bluetoothEntity) {
        setBluetoothEntity(bluetoothEntity);
        return this;
    }


    public CorrelationEntity setGetCluster(ClusterEntity cluster) {
        setCluster(cluster);
        return this;
    }

    public String toString(){
        return "CorrelationEntity("+getDatetime()+", "+getCluster()+", "+getBluetoothEntity()+")";
    }
}
