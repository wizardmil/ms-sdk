package seom.internal.storage.room.databases.processing.correlation;

import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import java.util.Date;
import java.util.List;

import seom.controller.CONFIG;
import seom.internal.storage.room.databases.sensing.location.LocationRepository;
import seom.internal.storage.room.entities.BluetoothEntity;
import seom.internal.storage.room.entities.CorrelationEntity;
import seom.internal.storage.room.entities.ClusterEntity;

public class CorrelationRepository {

    private final String DB_NAME = "PROCESSED_CORRELATION";

    private static CorrelationDatabase correlationDatabase;

    private static CorrelationRepository instance;

    public int rowCount;

    public CorrelationRepository(){
        super();
        correlationDatabase = Room.databaseBuilder(CONFIG.CONTEXT, CorrelationDatabase.class, DB_NAME).build();
    }

    public static CorrelationRepository getInstance(){
        if (instance == null){
            instance = new CorrelationRepository();
        }
        return instance;
    }

    public void insertTask(List<BluetoothEntity> blueToothEntities, ClusterEntity cluster) {
        insertTask(new CorrelationEntity().setGetBluetoothEntity(blueToothEntities).setGetCluster(cluster).setGetDatetime(CONFIG.DATE_FORMAT.format(new Date())));
    }

    public void delteButKeep(final int amount_to_keep){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Log.d("debugging-storage", "db size: " +correlationDatabase.daoAccess().getRowCount()+"");
                    int count =  correlationDatabase.daoAccess().getRowCount();
                    List<Integer> ids = correlationDatabase.daoAccess().getIds(count);
                    String keep = "";

                    int g = ids.size() - amount_to_keep;

                    if (g > 0){
                        for (int i = 0; i < g; i++){
                            keep += ids.get(i)+"";
                            if (i != g-1){
                                keep += ",";
                            }
                        }
                        Log.d("debugging-storage", keep);
                        correlationDatabase.daoAccess().purgeAfterOffset(keep);
                        Log.d("debugging-storage", "db size: " +correlationDatabase.daoAccess().getRowCount()+"");
                    }

                } catch (NullPointerException e){
                    Log.d("debugging-storage", e.toString());
                }
                return null;
            }
        }.execute();
    }

    public LiveData<Integer> getCount() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                CorrelationRepository.getInstance().rowCount = correlationDatabase.daoAccess().getRowCount();
                return null;
            }

        }.execute();

        return LocationRepository.getInstance().rowCount;
    }

    public void insertTask(final CorrelationEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids){
                correlationDatabase.daoAccess().inserTask(node);
                return null;
            }

        }.execute();
    }

    public void updateTask(final CorrelationEntity node) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                correlationDatabase.daoAccess().updateTask(node);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final int id) {
        final LiveData<CorrelationEntity> task = getTask(id);
        if(task != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    correlationDatabase.daoAccess().deleteTask(task.getValue());
                    return null;
                }
            }.execute();
        }
    }

    public void deleteTask(final CorrelationEntity note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                correlationDatabase.daoAccess().deleteTask(note);
                return null;
            }
        }.execute();
    }

    public LiveData<CorrelationEntity> getTask(int id) {
        return correlationDatabase.daoAccess().getTask(id);
    }

    public List<CorrelationEntity> getTasks() {
        return correlationDatabase.daoAccess().fetchAllEntities();
    }

    public List<CorrelationEntity> getNumberOfTasks(int size){

        return correlationDatabase.daoAccess().fetchNumberOfEntities(size);
    }

    /**
     * IMPORTANT! MUST NOT BE CALLED ON UI THREAD
     */
    public void enableTotalTableMeltdown(){
        correlationDatabase.daoAccess().purgeTable();
    }
}
