package seom.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import seom.resolvers.SensingStrategyResolver;

public class SensingRestarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Strategy restarted", Toast.LENGTH_SHORT).show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, SensingStrategyResolver.getClassCorrelation()));
        } else {
            context.startService(new Intent(context, SensingStrategyResolver.getClassCorrelation()));
        }

        Log.d("DEBUGGING-INFO", "RESTARTING");
    }
}
