package seom.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import seom.activities.DemoActivity;

public class StartActivityOnBootReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals((intent.getAction()))){
            Intent i = new Intent(context, DemoActivity.class);
            i.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
