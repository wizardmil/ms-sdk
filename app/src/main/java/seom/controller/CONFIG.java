package seom.controller;

import android.content.Context;

import java.text.SimpleDateFormat;

public class CONFIG {
    /**
     * This class is used for GLOBAL configurations
     * Try to avoid them as much as possible.
     *
     * These are, however, used as settings.
     */

    /* GLOBAL Context for all service broadcasts, must be used very careful */
    public static Context CONTEXT;
    public static Context ACTIVITY;

    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    /* These variables are used to keep track of the battery */
    public static int CURRENT_BATTERY = -1;
    public static int START_BATTERY = -1;


    /* For obscurring processed such that they look like something else than what they are, this refers to SensingChannels */
    public static boolean OBSCURE = false;
    public static boolean MEMORY_ENABLED = false;

    /* If true, the processing strategies will perfomr mean-filtering on the measured data */
    public static boolean DO_SIGNAL_FILTERING = false;

    /* If above 0, the processing strategies will suppress the data equal to k-anonymety TODO: NOT YET IMPLEMENTED*/
    public static int K_ANONYMETY = 0;

    /* Global for processing strategies*/
    public static int LOCATION_BATCHSIZE_PROCESSING;
    public static int LOCATION_SAMPLINGRATE_PROCESSING;

    /* Global for processing strategies TODO: SENSING_LIGHTING NOT YET IMPLEMENTED*/
    public static int LOCATION_BATCHSIZE_SENSING;
    public static int LOCATION_SAMPLINGRATE_SENSING;

    public static int LIGHTING_BATCHSIZE_SENSING;
    public static int LIGHTING_SAMPLINGRATE_SENSING;

    /* Global for processing strategies*/
    public static int BLUETOOTH_BATCHSIZE_SENSING;
    public static int BLUETOOTH_SAMPLINGRATE_SENSING;

    public static int CORRELATION_BATCHSIZE_PROCESSING;
    public static int CORRELATION_SAMPLINGRATE_PROCESSING;

    public static final String LOCATION_CLEAN = "android.background.service.sensing.location.network";
    public static final String LOCATION_OBSCURED = "android.native.service.timer";

    public static final String LOCATION_PROCESSING_CLEAN = "android.background.service.processing.location";
    public static final String LOCATION_PROCESSING_OBSCURED = "android.native.service.alarmclock";

    public static final String LIGHTING_OBSCURED = "android.native.service.light";
    public static final String LIGHTING_CLEAN = "android.native.service.sensing.illuminance";

    public static final String BLUETOOTH_CLEAN = "android.background.service.sensing.bluetooth";
    public static final String BLUETOOTH_OBSCURED = "android.native.service.batterylevel";
}
