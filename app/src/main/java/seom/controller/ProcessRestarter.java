package seom.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import seom.resolvers.ProcessingStrategyResolver;

public class ProcessRestarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Process restarted", Toast.LENGTH_SHORT).show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, ProcessingStrategyResolver.getClassCorrelation()));
        } else {
            context.startService(new Intent(context, ProcessingStrategyResolver.getClassCorrelation()));
        }
    }
}
