package seom.controller;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

public class ServiceStarter {

    /**
     * initService()
     * Starting a foreground service differently in relation to builds
     * Anything greater than OREO requires strict notification policies
     */
    public static void initService(String NOTIFICATION_CHANNEL_ID, String CHANNEL_NAME, Service service, int color, String contentTitle){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O){
            startService(NOTIFICATION_CHANNEL_ID, CHANNEL_NAME, service, color, contentTitle);
        } else {
            service.startForeground(1, new Notification());
        }
    }

    /**
     * Creates a channel for the backgroundservices (strategies)
     * based on the parameters.
     * @param NOTIFICATION_CHANNEL_ID
     * @param CHANNEL_NAME
     * @param service
     * @param color
     */
    @RequiresApi(Build.VERSION_CODES.O)
    public static void startService(String NOTIFICATION_CHANNEL_ID, String CHANNEL_NAME, Service service, int color, String contentTitle){
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
        channel.setLightColor(color);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationManager manager = (NotificationManager) service.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        manager.createNotificationChannel(channel);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(service.getApplicationContext(), NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle(contentTitle)
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        service.startForeground(2, notification);

    }
}
