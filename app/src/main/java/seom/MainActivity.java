package seom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import seom.activities.DemoActivity;


public class MainActivity extends AppCompatActivity {

    /**
     * The application starts with this activity
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, DemoActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}

