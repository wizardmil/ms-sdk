package seom;

import android.app.Activity;
import android.util.Log;
import android.util.Pair;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import seom.controller.CONFIG;
import seom.controller.DataType;
import seom.exceptions.IncorrectDataTypeException;
import seom.exceptions.OutOfBoundsPrecision;
import seom.internal.storage.room.databases.processing.ProcessedLocRepository;
import seom.internal.storage.room.databases.processing.correlation.CorrelationRepository;
import seom.internal.storage.room.databases.sensing.bluetooth.BluetoothRepository;
import seom.internal.storage.room.entities.BluetoothEntity;
import seom.internal.storage.room.entities.ClusterEntity;
import seom.internal.storage.room.entities.CorrelationEntity;
import seom.managers.ExportManager;
import seom.managers.ProcessingManager;
import seom.managers.SensingManager;
import seom.managers.StorageManager;
import seom.objects.Valid;
import seom.resolvers.SensingStrategyResolver;
import seom.sensing.AbstractSensingStrategy;
import seom.storage.IStrategyListener;

public class Mole {

    private SensingManager snm = null;
    private ProcessingManager prm = null;
    private IStrategyListener storageStrategy = null;

    private int sensing_sampling_rate = 500;
    private int sensing_batch_size = 5;
    private int processing_wait_time = 2;

    private Activity activity;

    public Mole(){
        Log.d("STARTED-HEEH", "testing");
    }

    public Mole(Activity activity, int sampling_rate, int batch_size, int wait_cycles){
        this.activity = activity;
        sensing_sampling_rate = sampling_rate;
        sensing_batch_size = batch_size;
        processing_wait_time = wait_cycles;


        snm = new SensingManager(this.activity);
        prm = new ProcessingManager(sensing_batch_size*sensing_sampling_rate*processing_wait_time);
    }

    public Mole(Activity activity){
        this.activity = activity;
    }

    public Mole setEnvironmentStorageStrategy(IStrategyListener storageStrategy){
        this.storageStrategy = storageStrategy;
        return this;
    }

    public Mole initializeSensing(DataType type){
        return initializeSensing(type, storageStrategy);
    }

    public Mole initializeCustomSensingStrategy(AbstractSensingStrategy strategy, IStrategyListener listener, String name, String channel){
        SensingStrategyResolver.initializeGenericService(listener, strategy, name, channel);
        return this;
    }

    public Mole initializeCustomSensingStrategy(AbstractSensingStrategy strategy, String name, String channel){
        if(storageStrategy!=null){
            SensingStrategyResolver.initializeGenericService(storageStrategy, strategy, name, channel);
        }
        return this;
    }

    public Mole initializeSensing(DataType type, IStrategyListener storageStrategy){
        initializeSensing(type, storageStrategy, sensing_batch_size);
        return this;
    }

    public Mole initializeSensing(DataType type, int batch_size){
        initializeSensing(type, storageStrategy, batch_size);
        return this;
    }

    public Mole initializeSensing(DataType type, IStrategyListener storageStrategy, int batch_size){
        if (storageStrategy != null){
            if (type == DataType.SENSING_LOCATION){
                snm.initializeLocationSensing(storageStrategy, batch_size, sensing_sampling_rate);
            } else if (type == DataType.BATTERY){
                snm.initializeBatterySensing(storageStrategy);
            } else if (type == DataType.SENSING_BLUETOOTH){
                snm.initializeBluetoothSensing(storageStrategy, batch_size);
            } else if (type == DataType.SENSING_LIGHTING){
                snm.initializeLightSensing(storageStrategy, batch_size, sensing_sampling_rate);
            } else if (type == DataType.SENSING_SOUND){
                // TODO: Implement sound sensing strategy
            } else {
                try {
                    throw new IncorrectDataTypeException(DataType.UNKNOWN, DataType.ANY);
                } catch (IncorrectDataTypeException e) {
                    e.printStackTrace();
                }
            }
        }

        return this;
    }

    public void deleteHistoricalData(DataType type, int id){
        StorageManager.deleteFromStorage(type, id);
    }

    public Object getHistoricalData(DataType type, int amount){
        return StorageManager.getDataFromStorage(type, amount);
    }

    public Object getHistoricalData(DataType type){
        return StorageManager.getDataFromStorage(type);
    }

    /**
     * Using the environment storage strategy and generic settings (i=10, k=20, bluetooth=3, clusters=20)
     * @param type
     * @return
     */
    public Mole processData(DataType type){
        if (storageStrategy!=null){
            if (DataType.PROCESSING_LOCATION == type){
                return processData(type, storageStrategy, new Integer[]{3, 10});
            } else if (DataType.CORRELATE_BLUETOOTH_LOCATION == type){
                return processData(type, storageStrategy, new Integer[]{3, 20});
            } else {
                return this;
            }
        }
        return this;
    }

    /**
     * @param type - executes the correct process based on the type.
     * @param setting1 - can be either; k as a value in K-Means-Clustering (location processing), or amount of bluetooth devices to consider during location and bluetooth correlation.
     * @param setting2 - can be either; i as nr of iterations in K-Means-Clustering (location processing), or amount of clusters to consider during location and bluetooth correlation.
     * @return
     */
    public Mole processData(DataType type, int setting1, int setting2){
        if(storageStrategy!=null){
            return processData(type, storageStrategy, new Integer[]{setting1, setting2});
        }
        return this;
    }

    /**
     * Using a specially implemented storage listener, by the developer
     * @param type
     * @param storageStrategy
     * @return
     */
    public Mole processData(DataType type, IStrategyListener storageStrategy, Integer[] settings){
        if (storageStrategy!=null){
            if (type==DataType.PROCESSING_LOCATION){
                prm.processLocationDataFromStorage(storageStrategy, sensing_batch_size*processing_wait_time, settings[0], settings[1]);
            } else if(type==DataType.CORRELATE_BLUETOOTH_LOCATION){
                prm.correlateLocationAndBlueTooth(settings[0], settings[1]);
            }
        }
        return this;
    }

    public int getStartBatteryLevel(){
        return CONFIG.START_BATTERY;
    }

    public int getCurrentBatteryLevel(){
        return CONFIG.CURRENT_BATTERY;
    }

    public Mole initializeSensingManager(){
        if (snm==null){
            snm = new SensingManager(this.activity, sensing_sampling_rate);
        }
        return this;
    }

    public Mole initializeProcessingManager(){
        if (prm==null){
            prm = new ProcessingManager(sensing_batch_size*sensing_sampling_rate*processing_wait_time);
        }
        return this;
    }

    /**
     * Used to enable/disable sensing strategies storing data in the phones memory
     * @return
     */
    public Mole flickMemory(){
        CONFIG.MEMORY_ENABLED = !CONFIG.MEMORY_ENABLED;
        return this;
    }

    public Mole enableSuppression(int k){
        ExportManager.getInstance().enableSuppression(k);
        return this;
    }

    public Mole exportDataset(DataType type){
        Thread exportThread = new Thread(() -> {
            ExportManager.getInstance().exportFromStorage(type);
        });
        exportThread.start();
        return this;
    }

    public Mole exportDataset(String filename, String formattedData){
        Thread exportThread = new Thread(() -> {
            ExportManager.getInstance().exportData(filename, formattedData);
        });
        exportThread.start();
        return this;
    }


    public SensingManager sensingManager(){
        return snm;
    }

    public ProcessingManager processingManager(){
        return prm;
    }

    public Mole setBatchSize(int size){
        this.sensing_batch_size = size;
        return this;
    }

    public Mole setSamplingRate(int size){
        this.sensing_sampling_rate = size;
        return this;
    }

    public Mole setWaitTime(int size){
        this.processing_wait_time = size;
        return this;
    }

    public int getBatchSize(){
        return this.sensing_batch_size;
    }

    public int getSamplingRate(){
        return this.sensing_sampling_rate;
    }

    public int getWaitTime(){
        return this.processing_wait_time;
    }


    /**
     * Generic method for getting a location estimate based on whatever is available
     * @return
     */
    public List<String> getEitherLocationEstimate(int scope){
        Pair<Integer, Integer> p = calculatePrecision(scope);


        Valid estimate = new Valid(); // getLocationBasedOnBluetooth(p.first);
        if (estimate.isNull()){
            estimate = getLocationBasedOnTime(p.second);
        }
        return estimate.normalize().getAsList();
    }

    private Pair<Integer, Integer> calculatePrecision(int precision){
        int i = 0;
        int k = 0;
        if(precision<=0 || precision>10){
            try {
                throw new OutOfBoundsPrecision();
            } catch (OutOfBoundsPrecision outOfBoundsPrecision) {
                outOfBoundsPrecision.printStackTrace();
            }
        } else {
            i = 2*precision;
            k = 5*precision;
        }

        return new Pair<>(i,k);
    }

    /**
     * Method for getting a fused location estimate
     * @return
     */
    public List<String> getFusedLocationEstimate(int scope){
        Pair<Integer, Integer> p = calculatePrecision(scope);
        Valid bluetooth = getLocationBasedOnBluetooth(p.first);
        Valid time = getLocationBasedOnTime(p.second);
        return bluetooth.normalize().merge(time.normalize()).normalize().getAsList();
    }


    /**
     * This is the function that correlates bluetooth with the stored cluster locations
     * and figures
     * @return
     */
    public Valid getLocationBasedOnBluetooth(int i){
        List<BluetoothEntity> devices = BluetoothRepository.getInstance().getNumberOfTasks(i);
        List<CorrelationEntity> correlationEntities = CorrelationRepository.getInstance().getTasks();
        Valid valid = new Valid();
        for (CorrelationEntity a : correlationEntities){
            Float score = 0f;
            for(BluetoothEntity b : devices){
                if (a.getComparableBluetoothEntities().contains(b.getCompareableString())) score+=1;
            }
            valid.add(a.getCluster(), score);
        }
        return valid;
    }


    /**
     * This is the function that returns a location estimate based on the time of day
     * @return
     */
    public Valid getLocationBasedOnTime(int k){
        Date now = new Date();
        Valid valid = new Valid();
        List<ClusterEntity> consider = new ArrayList<>();
        List<ClusterEntity> clusterEntities = ProcessedLocRepository.getInstance().getNumberOfTasks(k);

        if (clusterEntities==null){
            return valid;

        }
        for(ClusterEntity p : clusterEntities){
            Date start;
            Date end;
            try {
                start = CONFIG.DATE_FORMAT.parse(p.getStartTime());
                end = CONFIG.DATE_FORMAT.parse(p.getEndTime());

                if (start.getHours() <= now.getHours() && end.getHours() >= now.getHours()){
                    consider.add(p);
                }
            } catch (ParseException | NullPointerException ex) {
                continue;
            }
        }
        for (ClusterEntity a : consider){
            if(valid.contains(a.toComparableString())){
                valid.increaseScore(a.toComparableString());
                continue;
            }
            valid.add(a.toComparableString());
        }

        return valid;
    }

    /**
     * Destroys all running strategies, whether process or sensing
     */
    public void cleanUP(){
        snm.cleanUp();
        prm.cleanUP();
    }
}
