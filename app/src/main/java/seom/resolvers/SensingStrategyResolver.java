package seom.resolvers;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;

import seom.controller.DataType;
import java.util.ArrayList;
import java.util.List;

import seom.controller.CONFIG;
import seom.sensing.AbstractSensingStrategy;
import seom.sensing.battery.BatterySensingStrategy;
import seom.sensing.bluetooth.BluetoothSniffingStrategy;
import seom.sensing.lighting.LightSensingStrategy;
import seom.sensing.location.LocationGPSStrategy;
import seom.storage.IStrategyListener;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.BLUETOOTH;
import static android.Manifest.permission.BLUETOOTH_ADMIN;

public class SensingStrategyResolver  {

    /*
    This is used to keep track of the services that are meant to restart.
     */
    private static ArrayList<Class> restartingSensingStrategies = new ArrayList<>();
    private static ArrayList<AbstractSensingStrategy> runningServices = new ArrayList<>();
    private static ArrayList<Intent> runningSIntents = new ArrayList<>();

    /**
     * Resolves which location strategy to use, based on the active permissions.
     * @param storageStrategy
     * @return
     */
    public static void resolveLocationStrategy(IStrategyListener storageStrategy) {
        int fine = CONFIG.CONTEXT.checkCallingOrSelfPermission(ACCESS_FINE_LOCATION);
        int coarse = CONFIG.CONTEXT.checkCallingOrSelfPermission(ACCESS_COARSE_LOCATION);

        Intent strategy = null;
        LocationGPSStrategy gpsStrategy = null;

        if (fine == PackageManager.PERMISSION_GRANTED || coarse == PackageManager.PERMISSION_GRANTED) {
            gpsStrategy = new LocationGPSStrategy();

            strategy = new Intent(CONFIG.CONTEXT, gpsStrategy.getClass());

            strategy.putExtra("NOTIFICATION_CHANNEL_ID", "strategy.sensing.location.gps");

            if (!CONFIG.OBSCURE){
                strategy.putExtra("CHANNEL_NAME", CONFIG.LOCATION_CLEAN);
            } else {
                strategy.putExtra("CHANNEL_NAME", CONFIG.LOCATION_OBSCURED);
            }
            strategy.putExtra("STORING_STRATEGY", storageStrategy);
        }

        initializeService(gpsStrategy, strategy);

    }

    public static void resolveBatterySensingStrategy(IStrategyListener storageStrategy){
        BatterySensingStrategy batteryStrategy = new BatterySensingStrategy();

        Intent strategy = new Intent(CONFIG.CONTEXT, batteryStrategy.getClass());

        strategy.putExtra("NOTIFICATION_CHANNEL_ID", "strategy.sensing.battery.strategy");

        if (!CONFIG.OBSCURE){
            strategy.putExtra("CHANNEL_NAME", "battery.monitor.clean");
        } else {
            strategy.putExtra("CHANNEL_NAME", "battery.monitor.obscured");
        }
        strategy.putExtra("STORING_STRATEGY", storageStrategy);

        initializeService(batteryStrategy, strategy);
    }

    /**
     * Resolves which illuminance strategy to use, based on the active permissions.
     * @param storageStrategy
     * @return
     */
    public static void resolveLightingStrategy(IStrategyListener storageStrategy){

        LightSensingStrategy iluStrategy = new LightSensingStrategy();

        Intent strategy = new Intent(CONFIG.CONTEXT, iluStrategy.getClass());

        strategy.putExtra("NOTIFICATION_CHANNEL_ID", "strategy.sensing.illuminance.strategy");

        if (!CONFIG.OBSCURE){
            strategy.putExtra("CHANNEL_NAME", CONFIG.LIGHTING_CLEAN);
        } else {
            strategy.putExtra("CHANNEL_NAME", CONFIG.LIGHTING_OBSCURED);
        }
        strategy.putExtra("STORING_STRATEGY", storageStrategy);

        initializeService(iluStrategy, strategy);
    }

    /**
     * Resolves which bluetooth strategy to use, based on the active permissions.
     * @param storageStrategy
     * @return
     */
    public static void resolveBluetootheStrategy(IStrategyListener storageStrategy) {
        //admin lets us turn on and off bluetooth by ourselves, without the user knowing
        int hidden = CONFIG.CONTEXT.checkCallingOrSelfPermission(BLUETOOTH_ADMIN);
        int plain = CONFIG.CONTEXT.checkCallingOrSelfPermission(BLUETOOTH);

        Intent strategy = null;
        BluetoothSniffingStrategy bluetoothStrategy = null;

        if (hidden == PackageManager.PERMISSION_GRANTED || plain == PackageManager.PERMISSION_GRANTED) {
            bluetoothStrategy = new BluetoothSniffingStrategy();

            strategy = new Intent(CONFIG.CONTEXT, bluetoothStrategy.getClass());

            strategy.putExtra("NOTIFICATION_CHANNEL_ID", "strategy.sensing.bluetooth");

            if (!CONFIG.OBSCURE){
                strategy.putExtra("CHANNEL_NAME", CONFIG.BLUETOOTH_CLEAN);
            } else {
                strategy.putExtra("CHANNEL_NAME", CONFIG.BLUETOOTH_OBSCURED);
            }
            strategy.putExtra("STORING_STRATEGY", storageStrategy);
        }

        initializeService(bluetoothStrategy, strategy);
    }

    /**
        This function can be used to initialize any sensing strategy implemented from
        the AbstractSensingStrategy.
        @param listener must implement a storage strategy listener
        @param strategy the implemented strategy
        @param channelName the android system channel name
        @param notificationId a notification id
     */
    public static void initializeGenericService(IStrategyListener listener, AbstractSensingStrategy strategy, String channelName, String notificationId){
        Intent intent = new Intent(CONFIG.CONTEXT, listener.getClass());
        intent.putExtra("NOTIFICATION_CHANNEL_ID", notificationId);
        intent.putExtra("CHANNEL_NAME", channelName);
        intent.putExtra("STORING_STRATEGY", listener);
        initializeService(strategy, intent);
    }

    /**
     * Initialisez a service and binds it to the application context.
     * This way any other instance can access its nested methods by calling getRunningService
     * @param strategy
     * @param intent
     */
    private static void initializeService(AbstractSensingStrategy strategy, Intent intent){
        // If a strategy was found with the current permissions, run the strategy.
        if (strategy != null) {
            if (!validateServiceRunning(strategy.getClass(), CONFIG.CONTEXT)) {


                /** Defines callbacks for service binding, passed to bindService() */
                ServiceConnection connection = new ServiceConnection() {

                    @Override
                    public void onServiceConnected(ComponentName className, IBinder service) {

                        AbstractSensingStrategy.LocalBinder binder = (AbstractSensingStrategy.LocalBinder) service;
                        runningServices.add(binder.getService());

                    }

                    @Override
                    public void onServiceDisconnected(ComponentName arg0) {
                    }
                };

                try {
                    CONFIG.CONTEXT.bindService(intent, connection, Context.BIND_AUTO_CREATE);
                } catch (Exception e){}

                CONFIG.CONTEXT.startService(intent);
                runningSIntents.add(intent);
            }
        }
    }

    /**
     * Adds a strategy class to the restart sensing strategies list
     * and is used by the AbstractSensingStrategy
     */
    public static void appendToRestart(Class restartingClass){
        restartingSensingStrategies.add(restartingClass);
    }

    /**
     * Returns and removes from restarting sensing strategies.
     * @return SensingStrategy class
     */
    public static Class getClassCorrelation(){
        Class cls = restartingSensingStrategies.get(0);
        restartingSensingStrategies.remove(0);
        Log.d("DEBUGGING-INFO", "SensingStrategyResolver: Found class correlation->" + cls + "");
        return cls;
    }

    /**
     * Returns the in memory dataobject
     * @param type
     * @param batchsize
     * @return
     */
    public static List<Pair<String, String>> getDataObjectFromServiceMemory(DataType type, int batchsize){
        return SensingStrategyResolver.getRunningService(type).getDataObject().getClearMemory(batchsize);
    }

    public static AbstractSensingStrategy getRunningService(DataType type){
        for(int i = 0; i < runningServices.size(); i++){
            if (runningServices.get(i).getDataObject().getDataType() == type){
                return runningServices.get(i);
            }
        }
        return null;
    }

    /**
     * Used to validate if a service is running or not.
     * This ensures that only one instance of sensing strategy is
     * running at the time.
     * @param serviceClass - e.g. LocationNetworkStrategy.class
     * @param context - e.g. MainActivity instance
     * @return
     */
    public static boolean validateServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Called by top-level onDestroy
     * @param context
     */
    public static void cleanUp(Context context){

        for (int i = 0; i < runningSIntents.size(); i++){
            context.stopService(runningSIntents.get(i));
            runningSIntents.remove(i);
            runningServices.remove(i);
        }
    }
}
