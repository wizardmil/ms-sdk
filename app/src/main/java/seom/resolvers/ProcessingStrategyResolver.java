package seom.resolvers;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;

import seom.controller.CONFIG;
import seom.controller.DataType;
import seom.processing.Bluetooth.CorrelationProcessing;
import seom.processing.IProcessingStrategy;
import seom.processing.location.LocationProcessingStrategy;
import seom.storage.IStrategyListener;

public class ProcessingStrategyResolver {

    /*
This is used to keep track of the services that are meant to restart.
 */
    private static ArrayList<Class> restartingSensingStrategies = new ArrayList<>();
    private static ArrayList<IProcessingStrategy> runningServices = new ArrayList<>();
    private static ArrayList<Intent> runningIntents = new ArrayList<>();


    public static void startCorrelating(int nr_blueTooth_devices_per_iteration, int nr_clusters_per_iteration){
        CorrelationProcessing correlationStrategy = new CorrelationProcessing();
        Intent correlationIntent = new Intent(CONFIG.CONTEXT, correlationStrategy.getClass());
        correlationIntent.putExtra("CHANNEL_NAME", "correlation.strategy");
        correlationIntent.putExtra("NOTIFICATION_CHANNEL_ID", "strategy.processing.location.bluetooth.correlation");
        correlationIntent.putExtra("K", nr_blueTooth_devices_per_iteration);
        correlationIntent.putExtra("I", nr_clusters_per_iteration);

        if (!validateServiceRunning(correlationIntent.getClass(), CONFIG.CONTEXT)) {
            CONFIG.CONTEXT.startService(correlationIntent);
            runningServices.add(correlationStrategy);
            runningIntents.add(correlationIntent);
            Log.d("debugging", "STARTED CORRELATION SERVICE");
        }
    }

    public static void processLocationData(boolean fromMemory, IStrategyListener storageStrategy, int k, int i){
        LocationProcessingStrategy locationStrategy = new LocationProcessingStrategy();
        Intent locationIntent = new Intent(CONFIG.CONTEXT, locationStrategy.getClass());
        locationIntent.putExtra("NOTIFICATION_CHANNEL_ID", "strategy.processing.location.strategy");

        if (!CONFIG.OBSCURE){
            locationIntent.putExtra("CHANNEL_NAME", CONFIG.LOCATION_PROCESSING_CLEAN);
        } else {
            locationIntent.putExtra("CHANNEL_NAME", CONFIG.LOCATION_PROCESSING_OBSCURED);
        }

        locationIntent.putExtra("STORING_STRATEGY", storageStrategy);
        locationIntent.putExtra("K", k);
        locationIntent.putExtra("I", i);

        if (!fromMemory){

            locationIntent.putExtra("FROM_MEMORY", false);
            locationIntent.putExtra("FROM_STORAGE", true);

        } else if (fromMemory){
            locationIntent.putExtra("K", k);
            locationIntent.putExtra("I", i);

        }

        if (!validateServiceRunning(locationIntent.getClass(), CONFIG.CONTEXT)) {
            CONFIG.CONTEXT.startService(locationIntent);
            runningServices.add(locationStrategy);
            runningIntents.add(locationIntent);
        }

    }

    /**
     * Adds a strategy class to the restart sensing strategies list
     * and is used by the AbstractSensingStrategy
     */
    public static void appendToRestart(Class restartingClass){
        restartingSensingStrategies.add(restartingClass);
    }

    /**
     * Returns and removes from restarting sensing strategies.
     * @return SensingStrategy class
     */
    public static Class getClassCorrelation(){
        Class cls = restartingSensingStrategies.get(0);
        restartingSensingStrategies.remove(0);
        return cls;
    }

    /**
     * Used to validate if a service is running or not.
     * This ensures that only one instance of sensing strategy is
     * running at the time.
     * @param serviceClass - e.g. LocationNetworkStrategy.class
     * @param context - e.g. MainActivity instance
     * @return
     */
    public static boolean validateServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean validateProcessingStarted(DataType process){
        for (IProcessingStrategy i : runningServices){
            if (i.getProcessingObject().getDataType() == process){
                return true;
            }
        }
        return false;
    }

    /**
     * Called by top-level onDestroy
     * @param context
     */
    public static void cleanUp(Context context){

        for (int i = 0; i < runningIntents.size(); i++){
            context.stopService(runningIntents.get(i));
            runningServices.remove(i);
            runningIntents.remove(i);
        }
    }
}
