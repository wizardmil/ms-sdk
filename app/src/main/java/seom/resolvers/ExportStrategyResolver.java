package seom.resolvers;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import seom.internal.storage.room.databases.processing.ProcessedLocRepository;
import seom.internal.storage.room.databases.sensing.bluetooth.BluetoothRepository;
import seom.internal.storage.room.databases.sensing.location.LocationRepository;
import seom.internal.storage.room.entities.BluetoothEntity;
import seom.internal.storage.room.entities.ClusterEntity;
import seom.internal.storage.room.entities.LocationEntity;

public class ExportStrategyResolver {

    private static FileWriter writer;
    private static File root;
    private static File file;

    private static ExportStrategyResolver instance;

    public ExportStrategyResolver(){}

    public static ExportStrategyResolver getInstance(){
        if (instance == null){
            instance = new ExportStrategyResolver();
        }
        return instance;
    }

    /**
     * Is the method called from the ExportManager when exporting location data.
     * It retrieves data and if there are any, builds a string from it and writes it to a
     * csv file in external storage.
     */
    public void exportLocationData(){
        List<LocationEntity> rawLocationData =  LocationRepository.getInstance().getAllTasks();

        if (rawLocationData != null){
            String rawData = "ID,LATITUDE,LONGITUDE,DATETIME\n";
            for (int i = 0; i < rawLocationData.size(); i++){
                LocationEntity currentData = rawLocationData.get(i);
                rawData = rawData +  String.format("%d,%s,%s,%s\n", currentData.getId(), Double.toString(currentData.getLatitute()), Double.toString(currentData.getLongitute()), currentData.getDatetime());
                Log.d("DEBUGGING-INFO", "Writing object: " +currentData.toString() + " to csv format.");
            }
            saveExternally("LocationSensingData", rawData);
        }
    }

    public void exportClusterData(){
        List<ClusterEntity> processedLocationData =  ProcessedLocRepository.getInstance().getTasks();

        if (processedLocationData != null){

            String proData = "ID,BELONGING,ADDRESS_LINE,LOCALITY,ADMIN_AREA,COUNTRY_NAME,POSTAL_CODE,FEATURE_NAME,START_TIME,END_TIME,DATETIME\n";
            for (int i = 0; i < processedLocationData.size(); i++){
                ClusterEntity currentData = processedLocationData.get(i);
                Log.d("DEBUGGING-INFO", "Writing processing object: " + currentData.toString() + " to csv format.");
                proData = proData + String.format("%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", currentData.getId(),currentData.getBelonging(),currentData.getAddressLine(),currentData.getLocality(),currentData.getAdminArea(),currentData.getCountryName(),currentData.getPostalCode(),currentData.getFeatureName(),currentData.getStartTime(),currentData.getEndTime(),currentData.getDatetime());
            }
            saveExternally("ClusterData", proData);
        }
    }

    public void exportBlueToothData(){
        List<BluetoothEntity> devices =  BluetoothRepository.getInstance().getTasks();

        if (devices != null){

            String proData = "ID,NAME,ADDRESS,DATETIME\n";
            for (int i = 0; i < devices.size(); i++){
                BluetoothEntity currentData = devices.get(i);
                Log.d("DEBUGGING-INFO", "Writing bluetooth object: " + currentData.toString() + " to csv format.");
                proData = proData + String.format("%d,%s,%s,%s,\n", currentData.getId(), currentData.getName(), currentData.getMACAdress(), currentData.getDatetime());
            }
            saveExternally("BlueToothData", proData);
        }
    }

    public boolean saveExternally(String fileName, String data){
        if( Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            root = Environment.getExternalStorageDirectory();
            file = new File(root, fileName+".csv");


            try {

                writer = new FileWriter(file);

                writer.write(data);

                writer.flush();
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }



            Log.d("DEBUGGING-INFO", "Files Successfully exported.");
            return true;
        }
        return false;
    }

    public void clearDB(){
        LocationRepository.getInstance().enableTotalTableMeltdown();
        ProcessedLocRepository.getInstance().enableTotalTableMeltdown();

    }
}
