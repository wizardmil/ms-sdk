package seom.objects;

import android.util.Pair;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Valid {

    private HashMap<String, Float> data;


    public Valid(){
        data = new HashMap<>();
    }

    public void add(String data, Float score) {
        this.data.put(data, score);
    }

    public void add(String data) {
        this.data.put(data, 0f);
    }

    public void increaseScore(String data) {
        if (this.data.keySet().contains(data)) {
            Float score = this.data.get(data);
            this.data.put(data, score + 1f);
        }
    }

    public Set<String> getData() {
        return this.data.keySet();
    }

    public Collection<Float> getScores() {
        return this.data.values();
    }

    public Pair<String, Float> getHighestScore(){
        Float highest = 0f;
        String data = "";
        for (String key : this.data.keySet()){
            if (this.data.get(key) >= highest){
                highest = this.data.get(key);
                data = key;
            }
        }

        return new Pair<>(data, highest*100);
    }

    public Valid normalize(){
        Float highest = getHighestScore().second;
        HashMap<String, Float> normalized = new HashMap<>();
        for (String key : this.data.keySet()){
            normalized.put(key, this.data.get(key)/highest);
        }
        this.data.clear();
        this.data = normalized;
        return this;
    }

    private HashMap<String, Float> getMapping(){
        return this.data;
    }

    public Valid merge(Valid valid1){
        HashMap<String, Float> other = valid1.getMapping();
        for (String key : other.keySet()){
            if (this.data.keySet().contains(key)) {
                Float score = this.data.get(key);
                this.data.put(key, other.get(key)+score);
            } else {
                this.data.put(key, other.get(key));
            }
        }
        return this;
    }

    public boolean contains(String data){
        if (this.data.keySet().contains(data)) {
            return true;
        }
        return false;
    }

    public List<String> getAsList(){
        Pair<String, Float> highest = getHighestScore();
        return Arrays.asList(highest.first.split(","));
    }

    public int compare(String str1, String str2){
        String[] s1 = str1.split(",");
        String[] s2 = str2.split(",");

        int score = 0;

        int compare_length = 0;
        if (s1.length < s2.length){
            compare_length = s1.length;
        } else {
            compare_length = s2.length;
        }

        for (int i = 0; i < compare_length; i++){
            if (s1[i].equals(s2[i])){
                score++;
            }
        }

        return score;
    }

    public boolean isNull(){
        if (this.data.isEmpty()){
            return true;
        }
        return false;
    }

    public String toString(){
        return this.data.toString();
    }
}
