package seom.objects;

import android.util.Log;
import android.util.Pair;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import seom.controller.CONFIG;
import seom.controller.DataType;

public class DataObject implements Serializable {

    public DataType dataType;
    private List<Pair<String, String>> data;
    private List<Pair<String, String>> memory;

    public DataObject(DataType type){
        dataType = type;
    }

    /***
     *
     * @DataType is an enum that indicates the type of sensor data
     * storing strategies and further, should use this to base their
     * methods upon
     */
    public DataType getDataType(){
        return dataType;
    }

    public List<Pair<String, String>> getData(){
        return data;
    }

    public List<Pair<String, String>> getClearMemory(int mininumDataToReturn){

        if (this.memory == null) {
            this.memory = new ArrayList<>();
        }

        if (this.memory.size() >= mininumDataToReturn) {
            List<Pair<String, String>> tempMemory = memory;
            memory = null;
            return tempMemory;
        }
        return null;
    }

    /***
     *
     * @newLine should be a string that includes the current data
     * to be appended. For each data vector there should be a comma
     * separating them.
     */
    public void addData(String newLine){
        // if no data exists, instantiate
        if (data == null || data.isEmpty()){
            data = new ArrayList<>();
        }
        data.add(
                new Pair<String, String>(
                        newLine,
                        CONFIG.DATE_FORMAT.format(new Date())
                ));
    }

    public int getBatchSize(){
        if (data == null) {
            return 0;
        }
        return data.size();
    }

    @Override
    public String toString(){
        return "DataObject("+data.toString()+", type:"+getDataType()+")";
    }

    public void reset(){
        if (data != null){
            if (this.memory == null){
                this.memory = new ArrayList<>();
            }
            if (CONFIG.MEMORY_ENABLED){
                this.memory.addAll(data);
            }
            data = null;
        }
    }

    public void enableAmnesia(){
        this.memory = null;
    }
}
