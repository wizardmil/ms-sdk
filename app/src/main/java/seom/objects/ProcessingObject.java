package seom.objects;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import seom.controller.DataType;

public class ProcessingObject {

    public DataType dataType;
    private List<Object> data;
    private HashMap<Integer, HashMap<String, List<String>>>  table;

    public ProcessingObject(DataType type){
        dataType = type;
    }

    /***
     *
     * @DataType is an enum that indicates the type of sensor data
     * storing strategies and further, should use this to base their
     * methods upon
     */
    public DataType getDataType(){
        return dataType;
    }

    public List<Object> getData(){
        if (data == null){
            data = new ArrayList<>();
        }
        return data;
    }

    /***
     *
     * @newLine should be a string that includes the current data
     * to be appended. For each data vector there should be a comma
     * separating them.
     */
    public void addData(Object dataObject){
        // if no data exists, instantiate
        if (data == null){
            data = new ArrayList<>();
        }
        data.add(dataObject);
    }

    public int getBatchSize(){
        if (data == null) {
            return 0;
        }
        return data.size();
    }

    public HashMap returnObjectAsHashMap(Object any){
        try {
            return (HashMap) any;
        } catch (Exception e){
            return null;
        }
    }

    public List returnObjectAsList(Object any){
        try {
            return (List) any;
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public String toString(){
        if(data == null){
            data = new ArrayList<>();
        }
        return "ProcessingObject("+data.toString()+")";
    }

    public void reset(){
        data = null;
    }
}
