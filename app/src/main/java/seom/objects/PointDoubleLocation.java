package seom.objects;

public class PointDoubleLocation {

    /**
     * This is a POINTS - location
     * But with doubles
     */
    private double x;
    private double y;

    private String start;
    private String end;

    public PointDoubleLocation(double x, double y, String start, String end){
        this.x = x;
        this.y = y;
        if (start=="")
            this.start = null;
        else
            this.start = start;

        if (end=="")
            this.end = null;
        else
            this.end = end;

    }

    /**
     * Calculates the euclidean distance between two 2D points
     * @param toLocation
     * @return
     */
    public double calculateEuclideanDistance(PointDoubleLocation toLocation){
        return Math.sqrt(Math.pow((x-toLocation.getX()), 2) + Math.pow((y - toLocation.getY()), 2));
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public String getStartTime(){ return this.start; }

    public String getEndTime() { return this.end; }

    /**
     * Cheack wether or not two locations are the same
     * use this instead of equals and ==
     * @param otherLocation
     * @return
     */
    public boolean sameLocation(PointDoubleLocation otherLocation){
        if (this.x == otherLocation.getX() && this.y == otherLocation.getY()){
            return true;
        }
        return false;
    }

    @Override
    public String toString(){
        return "PointDoubleLocation("+getX()+", "+getY()+", "+getStartTime()+", "+getEndTime()+")";
    }
}
