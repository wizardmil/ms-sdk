package seom.objects;



import android.util.Pair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class KMeansCollection {
    /**
     * KMeansCollection is an implementation K-means clustering
     * It utilized a 2D double point implementation, PointDoubleLocation.
     *
     * 1. generates k random points
     * 2. sorts the measured points into clusters by comparing the euclidean distance, where each cluster is expressed by a random point
     * 3. assings the k random points to be the mean of the k clusters
     * 4. repeats step 2 with the new k clusters
     * 5. compares the sum euclidean distances of all clusters
     * 6. returns the cluster with the smallest variance (the smallest score)
     *
     */

    private ArrayList<PointDoubleLocation> array;
    private Random random;
    private HashMap<PointDoubleLocation, List<PointDoubleLocation>> clusters; // gets reassigned over and over

    public KMeansCollection(){
        array = new ArrayList<>();
        random = new Random(); // used to generate random coordinates
    }

    public PointDoubleLocation get(int i){
        return array.get(i);
    }

    public int size(){
        return array.size();
    }

    public void add(PointDoubleLocation location){
        array.add(location);
    }

    /**
     * Gets the minimal location from the array of PLocations
     * @return the minimal location
     */
    public PointDoubleLocation getMinimalLocation(){
        double  minimal = array.get(0).getX() + array.get(0).getY();
        int     indexer = 0;

        for (int i = 0; i < array.size(); i++){

            double currentValue = array.get(i).getX() + array.get(i).getY();

            if (currentValue < minimal){
                minimal = currentValue;
                indexer = i;
            }
        }
        return array.get(indexer);
    }

    /**
     * Gets the maximal location from the array of PLocations
     * @return the maximal location
     */
    public PointDoubleLocation getMaximalLocation(){
        double  maximal = array.get(0).getX() + array.get(0).getY();
        int     indexer = 0;

        for (int i = 0; i < array.size(); i++){

            double currentValue = array.get(i).getX() + array.get(i).getY();

            if (currentValue > maximal){
                maximal = currentValue;
                indexer = i;
            }
        }
        return array.get(indexer);
    }

    /**
     * Generates 1 random location between a maximal and a minimal 2d point
     * @param first
     * @param second
     * @return
     */
    private PointDoubleLocation getRandomLocation(PointDoubleLocation first, PointDoubleLocation second){
        double xMin;
        double xMax;
        double yMin;
        double yMax;

        if (first.getX() < second.getX()){
            xMin = first.getX();
            xMax = second.getX();
        } else {
            xMin = second.getX();
            xMax = first.getX();
        }

        if (first.getY() < second.getY()){
            yMin = first.getY();
            yMax = second.getY();
        } else {
            yMin = second.getY();
            yMax = first.getY();
        }
        return new PointDoubleLocation(xMin + (xMax - xMin) * random.nextDouble(), yMin + (yMax - yMin) * random.nextDouble(),"","");
    }

    /**
     * Utilizes getRandomLocation to generate k number of random locations
     * between a maximal and minimal 2D point
     * @param k
     * @return
     */
    private ArrayList<PointDoubleLocation> generateKrandomLocations(int k){

        PointDoubleLocation maximal = getMaximalLocation();
        PointDoubleLocation minimal = getMinimalLocation();

        ArrayList<PointDoubleLocation> randomLocations = new ArrayList<>();

        for (int i = 0; i < k; i++){
            randomLocations.add(getRandomLocation(minimal, maximal));
        }
        return randomLocations;
    }

    /**
     * This methods creates the existing number of clustes and assigns each measured location to the closest one.
     * @return
     */
    private double assignLocationsToClusters(){

        ArrayList<Double> score = new ArrayList<>();

        for (PointDoubleLocation knownLocation : array){
            double minimalDistance =  ((PointDoubleLocation)clusters.keySet().toArray()[0]).calculateEuclideanDistance(knownLocation);
            PointDoubleLocation minimalLocation = ((PointDoubleLocation) clusters.keySet().toArray()[0]);

            for (PointDoubleLocation randomLoc : clusters.keySet()){
                double currentDistance = randomLoc.calculateEuclideanDistance(knownLocation);
                if (currentDistance < minimalDistance){
                    minimalDistance = currentDistance;
                    minimalLocation = randomLoc;
                }
            }

            clusters.get(minimalLocation).add(knownLocation);
            score.add(minimalDistance);
        }

        double sum = 0;
        for(Double d : score)
            sum += d;

        return sum;
    }

    /** This methods calculates the mean of each existing clusters and assings the mean location to the random location */
    private void calculateMeanOfClusters(){

        HashMap<PointDoubleLocation, List<PointDoubleLocation>> newClusters = new HashMap<>();

        for (PointDoubleLocation randomLoc : clusters.keySet()){
            ArrayList<Double> xs = new ArrayList<>();
            ArrayList<Double> ys = new ArrayList<>();

            for (int i = 0; i < clusters.get(randomLoc).size(); i++){
                xs.add(clusters.get(randomLoc).get(i).getX());
                ys.add(clusters.get(randomLoc).get(i).getY());
            }

            if (xs.size() == 0){
                continue;
            }
            /*
            Calculating the summation of all x coordinates in the current cluster
             */
            double sumx = 0;
            for(Double d : xs)
                sumx += d;

            /*
            Calculating the summation of all y coordinates in the current cluster
             */
            double sumy = 0;
            for(Double d : ys)
                sumy += d;

            /*
                Creates the new clusters with the cluster nodes as the mean of each cluster
             */
            newClusters.put(new PointDoubleLocation(sumx / xs.size(), sumy / ys.size(), "", ""), new ArrayList<PointDoubleLocation>());
        }

        clusters = newClusters;
        //System.out.println("clusters: " + clusters.keySet().toString());
    }

    private void generateKClusters(int k){
        clusters = new HashMap<>();
        for (PointDoubleLocation randomLoc : generateKrandomLocations(k)){
            clusters.put(randomLoc, new ArrayList<PointDoubleLocation>());
        }
    }

    /**
     * MOTHER METHOD
     * Is called from instance
     * @param k
     * @param iterations
     * @return
     */
    public HashMap<String, PointDoubleLocation> calculateKMeans(int k, int iterations){
        ArrayList<Double> scores = new ArrayList<>();
        ArrayList<HashMap<PointDoubleLocation, List<PointDoubleLocation>>> finalClusters = new ArrayList<>();

        for (int i = 0; i < iterations; i++){
            generateKClusters(k);
            assignLocationsToClusters();

            double currentScore = 0;

            for (int j = 0; j < 15; j++){
                calculateMeanOfClusters();
                currentScore = assignLocationsToClusters();
            }

            scores.add(currentScore);
            finalClusters.add(clusters);
        }

        double minScore = scores.get(0);
        HashMap<PointDoubleLocation, List<PointDoubleLocation>> bestClusters = finalClusters.get(0);
        for (int i = 0; i < scores.size(); i++){
            if (scores.get(i) < minScore){
                minScore = scores.get(i);
                bestClusters = finalClusters.get(i);
            }
        }

        List<PointDoubleLocation> fnl = sortBySize(bestClusters);

        HashMap<String, PointDoubleLocation> returnClusters = new HashMap<>();

        for (int i = 0; i < fnl.size(); i++){
            String belonging = "";
            if (i == 0) belonging = "HOME";
            else if (i == 1) belonging = "WORK";
            else if (i == 2) belonging = "OTHER";
            else break;
            Pair<String, String> startEnd = extractStartAndEndTime(bestClusters.get(fnl.get(i)));
            returnClusters.put(belonging, new PointDoubleLocation(fnl.get(i).getX(), fnl.get(i).getY(), startEnd.first, startEnd.second));
        }

        return returnClusters;
    }

    private List<PointDoubleLocation> sortBySize(HashMap<PointDoubleLocation, List<PointDoubleLocation>> data){
        List<PointDoubleLocation> ret = new ArrayList<>();
        for (PointDoubleLocation a : data.keySet()){
            int highest = 0;
            PointDoubleLocation c = null;
            for (PointDoubleLocation b : data.keySet()){
                if (ret.contains(b)){
                    continue;
                }
                if (data.get(b).size()>=highest){
                    highest = data.get(b).size();
                    c = b;
                }
            }
            if (c!=null) ret.add(c);
        }

        return ret;
    }

    /**
     * Extracts the earliest and latest times of the start and end times
     * included in all the point double locations
     * @param content
     * @return
     */
    private Pair<String, String> extractStartAndEndTime(List<PointDoubleLocation> content){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        Date earliest = null;
        Date latest = null;
        for (PointDoubleLocation location : content){
            if (location.getStartTime() != null && location.getEndTime() != null){
                try {
                    Date start = format.parse(location.getStartTime());
                    Date end = format.parse(location.getEndTime());

                    if (earliest == null){
                        earliest = start;
                    } else if (earliest.getTime() > start.getTime()){
                        earliest = start;
                    }
                    if (latest == null){
                        latest = end;
                    } else if (latest.getTime() < end.getTime()){
                        latest = end;
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        Pair<String, String> startEnd = null;
        if (earliest != null && latest != null){
            startEnd = new Pair<>(format.format(earliest), format.format(latest));
        } else {
            startEnd = new Pair<>("NaN", "NaN");
        }
        return startEnd;
    }

    @Override
    public String toString(){
        return "KMeansClustering("+array.toString()+")";
    }
}



























