package seom.sensing.battery;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import seom.controller.CONFIG;
import seom.objects.DataObject;
import seom.sensing.AbstractSensingStrategy;

import seom.controller.DataType;

public class BatterySensingStrategy extends AbstractSensingStrategy {

    private Thread batteryThread;
    private static DataObject data;

    public BatterySensingStrategy(){
        super();
        data = new DataObject(DataType.BATTERY);
        BACH_SIZE_STORE_DATA = 1;

        batteryThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if(LOCAL_LAST_MEASUREMENT + 10000 <= System.currentTimeMillis()){

                        data.addData(getBatteryPercentage());

                        storeData();

                        data.enableAmnesia();

                        LOCAL_LAST_MEASUREMENT = System.currentTimeMillis();
                    }
                }
            }
        });

    }

    public void insertDataObject(DataObject dataObject){

    }

    public void storeData(){
        if (STORING_STRATEGY != null){
            if (data.getBatchSize() >= BACH_SIZE_STORE_DATA) {
                STORING_STRATEGY.onResponseDataObject(data);
                data.reset();
            }
        } else {
            data.reset();
        }
    }

    @Override
    public void sense() {
        batteryThread.start();
    }

    @Override
    public DataObject getDataObject() {
        return getStaticDataObject();
    }

    public static DataObject getStaticDataObject(){
        return data;
    }

    private String getBatteryPercentage()
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = CONFIG.CONTEXT.getApplicationContext().registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = level / (float)scale;
        float p = batteryPct * 100;

        return String.valueOf(Math.round(p));
    }
}
