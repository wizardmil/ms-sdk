package seom.sensing;

import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;

import seom.controller.SensingRestarter;
import seom.controller.ServiceStarter;
import seom.resolvers.SensingStrategyResolver;
import seom.storage.IStrategyListener;

/**
 * This class is trying to start any Sensing Strategy as a background service
 * that is not killed when the application is killed.
 *
 * We do this by launching a Broadcast in Android when the App is killed manually
 * and define a custom broadcast receiver to trigger a service to restart immediately after.
 */
public abstract class AbstractSensingStrategy extends Service implements ISensingStrategy {

    public String NOTIFICATION_CHANNEL_ID = null, CHANNEL_NAME = null;
    public IStrategyListener STORING_STRATEGY = null;

    public long LOCAL_LAST_MEASUREMENT = 0;

    public int BACH_SIZE_STORE_DATA = 0;


    private final IBinder binder;

    public AbstractSensingStrategy(){
        binder = new LocalBinder();
    }

    public class LocalBinder extends Binder {

        public AbstractSensingStrategy getService() {

            // Return this instance of AbstractSensingStrategy so clients can call public methods
            return AbstractSensingStrategy.this;
        }
    }

    /**
     * Gets called whenever the app is closed manually
     */
    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (CHANNEL_NAME == null && NOTIFICATION_CHANNEL_ID == null){

            Bundle serviceParams = intent.getExtras();

            try {
                STORING_STRATEGY = (IStrategyListener) serviceParams.get("STORING_STRATEGY");
            } catch (NullPointerException e){ }

            NOTIFICATION_CHANNEL_ID = (String) serviceParams.get("NOTIFICATION_CHANNEL_ID");
            CHANNEL_NAME = (String) serviceParams.get("CHANNEL_NAME");

            ServiceStarter.initService(NOTIFICATION_CHANNEL_ID, CHANNEL_NAME, this, Color.BLUE, "sensing.strategy."+CHANNEL_NAME);

            sense();
        }

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * onDestroy()
     * This class is quite important. It asynchronously sends a broadcast with
     * the acton name "restartservice", which is used as a trigger.
     * "restartservice" is declared in the manifest.
     */
    @Override
    public void onDestroy(){
        super.onDestroy();
        SensingStrategyResolver.appendToRestart(this.getClass());

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("sensinglistenerrestart");

        broadcastIntent.setClass(this, SensingRestarter.class);
        this.sendBroadcast(broadcastIntent);
    }

}
