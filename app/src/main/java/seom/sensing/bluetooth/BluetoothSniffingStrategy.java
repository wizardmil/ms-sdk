package seom.sensing.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.util.ArrayList;

import seom.controller.CONFIG;
import seom.controller.DataType;
import seom.internal.storage.room.databases.sensing.bluetooth.BluetoothRepository;
import seom.internal.storage.room.entities.BluetoothEntity;
import seom.objects.DataObject;
import seom.sensing.AbstractSensingStrategy;

public class BluetoothSniffingStrategy extends AbstractSensingStrategy {

//    private BluetoothSuccessListener bluetoothOnResponse;
    private Thread sensing;
    private boolean bluetoothOn = false;
    public static DataObject data;

    //checks for changes to the bluetooth receiver state
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        bluetoothOn = false;
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        bluetoothOn = false;
                        break;
                    case BluetoothAdapter.STATE_ON:
                        bluetoothOn = true;
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        //todo
                        break;
                }
            }
        }
    };

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                //todo handle getting data
                getStaticDataObject().addData(device.getName() + ","+ device.getAddress());
            }
        }
    };

    public BluetoothSniffingStrategy() {
        super();
        data = new DataObject(DataType.SENSING_BLUETOOTH);
        BACH_SIZE_STORE_DATA = CONFIG.LOCATION_BATCHSIZE_SENSING;

        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Log.d("TA", "BluetoothSniffingStrategy: WasNull AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            // Device doesn't support Bluetooth //todo
        } else {
            bluetoothOn = true;
            //todo check if bluetooth is eneabled
            sensing = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        if (bluetoothOn) {
                            if (LOCAL_LAST_MEASUREMENT + CONFIG.BLUETOOTH_SAMPLINGRATE_SENSING <= System.currentTimeMillis()) {
                                bluetoothAdapter.startDiscovery();

                                storeData();
                                LOCAL_LAST_MEASUREMENT = System.currentTimeMillis();
                            }
                        } else {
                            try {
                                Thread.sleep(10000);
                            } catch (InterruptedException e) {
                                //pass
                            }
                        }

                    }
                }
            });
        }
    }

    public void storeData(){
        if (STORING_STRATEGY != null){
            if (data.getBatchSize() >= BACH_SIZE_STORE_DATA) {
                STORING_STRATEGY.onResponseDataObject(data);
                data.reset();
            }
        } else {
            data.reset();
        }
    }

    @Override
    public void insertDataObject(DataObject dataObject){

        ArrayList<BluetoothEntity> entities = formatDataObject(dataObject);

        for (int i = 0; i < entities.size(); i++){
            BluetoothRepository.getInstance().insertTask(entities.get(i));
            Log.d("DEBUGGING-SENSING", "stored " + entities.size() + " bluetooth objects "+ entities.get(i).toString());
        }
    }

    private static ArrayList<BluetoothEntity> formatDataObject(DataObject dataObject){

        ArrayList<BluetoothEntity> entities = new ArrayList<>();

        for (int i = 0; i < dataObject.getBatchSize(); i++){

            entities.add(
                    new BluetoothEntity()
                            .setGetName(dataObject.getData().get(i).first.split(",")[0])
                            .setGetMACAdress(dataObject.getData().get(i).first.split(",")[1])
                            .setGetDatetime(dataObject.getData().get(i).second)
            );
        }

        return entities;
    }

    @Override
    public void onCreate() {
        IntentFilter mFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, mFilter);
        registerReceiver(receiver, filter);
        super.onCreate();
    }

    @Override
    public void sense() {
        if(sensing != null){
            sensing.start();
        }
    }

    @Override
    public DataObject getDataObject() {
        return getStaticDataObject();
    }

    public static DataObject getStaticDataObject(){
        return data;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        unregisterReceiver(receiver);
        super.onDestroy();
    }
};
