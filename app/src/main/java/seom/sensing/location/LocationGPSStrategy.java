package seom.sensing.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

import seom.controller.CONFIG;
import seom.controller.DataType;
import seom.objects.DataObject;
import seom.sensing.AbstractSensingStrategy;
import seom.internal.storage.room.databases.sensing.location.LocationRepository;
import seom.internal.storage.room.entities.LocationEntity;

public class LocationGPSStrategy extends AbstractSensingStrategy {

    private FusedLocationProviderClient locationClient;
    public static DataObject data;
    private LocationCallback lc;
    private int PRIORITY = -1;

    /**
     * This constructer is initialied by the abstract sensing strategy
     * It initializes a thread that runs in a loop and utilizes the storing technique (if provided)
     */
    public LocationGPSStrategy() {
        super();

        data = new DataObject(DataType.SENSING_LOCATION);
        BACH_SIZE_STORE_DATA = CONFIG.LOCATION_BATCHSIZE_SENSING;

        locationClient = LocationServices.getFusedLocationProviderClient(CONFIG.CONTEXT);
        lc = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                storeData();
                data.addData(locationResult.getLastLocation().getLatitude()+","+locationResult.getLastLocation().getLongitude());
                Log.d("debuggin-sensing", locationResult.getLastLocation().getLatitude()+","+locationResult.getLastLocation().getLongitude());
                Log.d("debuggin-sensing", "priority: " + PRIORITY);

                int new_priority = calculatePriority();
                if (new_priority!=-1){
                    if (PRIORITY!= calculatePriority()){
                        locationClient.removeLocationUpdates(lc);
                        startLocationUpdate();
                    }
                }
            }
        };
    }

    private int calculatePriority(){
        if(true){
            return LocationRequest.PRIORITY_HIGH_ACCURACY;
        }
        if (CONFIG.CURRENT_BATTERY>-1){
            if (CONFIG.CURRENT_BATTERY>=80){
                return LocationRequest.PRIORITY_HIGH_ACCURACY;
            } else if (CONFIG.CURRENT_BATTERY<80 && CONFIG.CURRENT_BATTERY>=60) {
                return LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
            } else if (CONFIG.CURRENT_BATTERY<60 && CONFIG.CURRENT_BATTERY>=35) {
                return LocationRequest.PRIORITY_LOW_POWER;
            } else {
                return LocationRequest.PRIORITY_NO_POWER;
            }
        }
        return LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
    }


    private LocationRequest createLocationRequest(){
        PRIORITY = calculatePriority();
        LocationRequest lr = new LocationRequest();
        lr.setInterval(CONFIG.LOCATION_SAMPLINGRATE_SENSING);
        lr.setFastestInterval(CONFIG.LOCATION_SAMPLINGRATE_SENSING);
        lr.setMaxWaitTime(CONFIG.LOCATION_SAMPLINGRATE_SENSING);
        lr.setPriority(PRIORITY);
        return lr;
    }

    private void startLocationUpdate() {
        locationClient.requestLocationUpdates(createLocationRequest(), lc, Looper.getMainLooper());
    }



    public void storeData(){
        if (STORING_STRATEGY != null){
            if (data.getBatchSize() >= BACH_SIZE_STORE_DATA) {
                STORING_STRATEGY.onResponseDataObject(data);
                data.reset();
            }
        } else {
            data.reset();
        }
    }

    @Override
    public void insertDataObject(DataObject dataObject){

        ArrayList<LocationEntity> entities = formatDataObject(dataObject);

        for (int i = 0; i < entities.size(); i++){
            LocationRepository.getInstance().insertTask(entities.get(i));
            Log.d("DEBUGGING-SENSING", "stored "+entities.size()+" new sensing objects: "+ entities.get(i).toString());
        }
    }

    private ArrayList<LocationEntity> formatDataObject(DataObject dataObject){

        ArrayList<LocationEntity> entities = new ArrayList<>();

        for (int i = 0; i < dataObject.getBatchSize(); i++){

            entities.add(
                    new LocationEntity()
                            .setGetLatitute(Double.parseDouble(dataObject.getData().get(i).first.split(",")[0]))
                            .setGetLongitute(Double.parseDouble(dataObject.getData().get(i).first.split(",")[1]))
                            .setGetDatetime(dataObject.getData().get(i).second)
            );
        }

        return entities;
    }

    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    public void sense() {
        startLocationUpdate();
    }

    @Override
    public DataObject getDataObject() {
        return getStaticDataObject();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    public static DataObject getStaticDataObject(){
        return data;
    }
}

class StartMyServiceAtBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, LocationGPSStrategy.class);
            context.startService(serviceIntent);
        }
    }
}