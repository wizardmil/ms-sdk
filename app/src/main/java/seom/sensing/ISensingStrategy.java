package seom.sensing;

import seom.objects.DataObject;

public interface ISensingStrategy {

    void sense();
    void storeData();
    void insertDataObject(DataObject dataObject);
    DataObject getDataObject();
}
