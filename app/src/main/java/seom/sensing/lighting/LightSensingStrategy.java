package seom.sensing.lighting;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import seom.controller.CONFIG;
import seom.controller.DataType;
import seom.objects.DataObject;
import seom.sensing.AbstractSensingStrategy;

public class LightSensingStrategy extends AbstractSensingStrategy {

    private LightSuccessListener lightOnResponse;
    private SensorManager sensorManager;
    private static DataObject data;
    private Thread sensing;
    private Sensor light;

    public LightSensingStrategy() {
        super();

        data = new DataObject(DataType.SENSING_LIGHTING);

        BACH_SIZE_STORE_DATA = CONFIG.LIGHTING_BATCHSIZE_SENSING;

        lightOnResponse = new LightSuccessListener();
        sensing = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (LOCAL_LAST_MEASUREMENT + CONFIG.LIGHTING_SAMPLINGRATE_SENSING <= System.currentTimeMillis()) {

                        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
                        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

                        sensorManager.registerListener(lightOnResponse, light,SensorManager.SENSOR_DELAY_NORMAL );


                        storeData();
                        Log.d("DEBUGGING-INFO", "illuminance sensing running");
                        LOCAL_LAST_MEASUREMENT = System.currentTimeMillis();
                    }

                }
            }
        });
    }

    public void insertDataObject(DataObject dataObject){

    }

    public void storeData(){
        if (STORING_STRATEGY != null){
            if (data.getBatchSize() >= BACH_SIZE_STORE_DATA) {
                STORING_STRATEGY.onResponseDataObject(data);
                data.reset();
            }
        } else {
            data.reset();
        }
    }

    @Override
    public void sense() {
        sensing.start();
    }

    @Override
    public DataObject getDataObject() {
        return getStaticDataObject();
    }

    public static DataObject getStaticDataObject(){
        return data;
    }
}

class LightSuccessListener implements SensorEventListener {

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_LIGHT){

            LightSensingStrategy.getStaticDataObject().addData(event.values[0] + "");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    }

