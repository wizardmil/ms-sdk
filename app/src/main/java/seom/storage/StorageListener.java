package seom.storage;

import seom.exceptions.IncorrectDataTypeException;
import seom.managers.StorageManager;
import seom.objects.DataObject;
import seom.objects.ProcessingObject;

public class StorageListener implements IStrategyListener {

    @Override
    public void onResponseDataObject(DataObject dataObject) {
        try {
            StorageManager.storeDataLocally(dataObject);
        } catch (IncorrectDataTypeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponseProcessingObject(ProcessingObject processingObject) {
        try {
            StorageManager.storeDataLocally(processingObject);
        } catch (IncorrectDataTypeException e) {
            e.printStackTrace();
        }
    }
}
