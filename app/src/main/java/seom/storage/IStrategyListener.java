package seom.storage;


import java.io.Serializable;

import seom.objects.DataObject;
import seom.objects.ProcessingObject;

public interface IStrategyListener extends Serializable {

    void onResponseDataObject(DataObject dataObject);
    void onResponseProcessingObject(ProcessingObject processingObject);

}
