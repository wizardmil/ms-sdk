package seom.activities;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import seom.controller.CONFIG;
import seom.controller.DataType;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import seom.main.R;
import seom.Mole;
import seom.storage.StorageListener;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.BLUETOOTH;
import static android.Manifest.permission.BLUETOOTH_ADMIN;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class DemoActivity extends AppCompatActivity {
    StorageListener storage;
    Mole mole;

    /**
     /**
     * Using the library with the following settings:
     *
     * bluetooth sensing
     * location sensing
     * location processing
     * @param savedInstanceState
     */


    private TextView demoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        demoTextView = (TextView) findViewById(R.id.storage_update_text);

        storage = new StorageListener();

        mole = new Mole(this, 30000, 10, 2)
                .setEnvironmentStorageStrategy(storage);

        mole.sensingManager()
                .requestPermission(new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, INTERNET, WRITE_EXTERNAL_STORAGE})
                .requestPermission(new String[]{BLUETOOTH, BLUETOOTH_ADMIN, ACCESS_FINE_LOCATION});

        mole.processingManager().enableSignalFiltering();

    }

    /** When the permissions are accepted, this method fires */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1) {

            mole.initializeSensing(DataType.BATTERY)
                    .initializeSensing(DataType.SENSING_LOCATION)
                    .initializeSensing(DataType.SENSING_BLUETOOTH, 10);

            //mole.processData(DataType.PROCESSING_LOCATION)
            //        .processData(DataType.CORRELATE_BLUETOOTH_LOCATION);
        }

        new Thread(() -> {

            while (true){
                runOnUiThread(() -> {
                    demoTextView.setText("Used "+(CONFIG.START_BATTERY - CONFIG.CURRENT_BATTERY)+"% so far. Current battery: "+CONFIG.CURRENT_BATTERY+"%");
                });
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     *
     * @param view
     */
    public void userInterfaceMethod(View view){


        if (view.getId() == R.id.estimatebtn){

            new Thread(() -> {
                List<String>  estimate = mole.getFusedLocationEstimate(5);
                if (estimate.isEmpty()){
                    estimate = mole.getEitherLocationEstimate(5);
                }
                List<String> finalEstimate = estimate;
                runOnUiThread(() -> {
                    TextView estimateTxtView = (TextView) findViewById(R.id.estimate);
                    estimateTxtView.setText(finalEstimate.toString());
                });

            }).start();
        }

        if (view.getId() == R.id.export_button){
            mole.exportDataset(DataType.SENSING_LOCATION).exportDataset(DataType.SENSING_BLUETOOTH);
            demoTextView = (TextView) findViewById(R.id.storage_update_text);
            demoTextView.setText(demoTextView.getText().toString() + "\n\n" + "Data successfully stored!");
        }
    }

    @Override
    protected void onDestroy(){
        mole.cleanUP();
        super.onDestroy();
    }

}
