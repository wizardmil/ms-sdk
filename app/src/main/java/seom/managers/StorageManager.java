package seom.managers;

import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import seom.controller.CONFIG;
import seom.controller.DataType;

import seom.exceptions.IncorrectDataTypeException;
import seom.internal.storage.room.databases.processing.ProcessedLocRepository;
import seom.internal.storage.room.databases.sensing.bluetooth.BluetoothRepository;
import seom.internal.storage.room.databases.sensing.light.LightRepository;
import seom.internal.storage.room.databases.sensing.location.LocationRepository;
import seom.objects.DataObject;
import seom.objects.ProcessingObject;
import seom.resolvers.SensingStrategyResolver;

public class StorageManager {

    /**
     * This is the storage manageer.
     * Every method must be static and should implement ways to store all sensing and processing strategies.
     *
     * Locally refers to ROOM
     * Externally refers to Server (must take server info as parameters)
     */


    /**
     * Automatically determines the storage strategy to be ueed based on the object
     * @param object
     * @throws IncorrectDataTypeException
     */
    public static void storeDataLocally(Object object) throws IncorrectDataTypeException{
        if (object instanceof DataObject){
            DataType type = ((DataObject) object).getDataType();
            if (type == DataType.SENSING_LOCATION){
                StorageManager.storeSensedLocationDataLocally((DataObject) object);
            }else if (type == DataType.SENSING_BLUETOOTH){
                StorageManager.storeSensedBluetoothDataLocally((DataObject) object);
            }else if (type == DataType.BATTERY) {
                int batteryLevel = Integer.parseInt(((DataObject) object).getData().get(0).first);
                if (CONFIG.START_BATTERY == -1) {
                    CONFIG.START_BATTERY = batteryLevel;
                }
                CONFIG.CURRENT_BATTERY = batteryLevel;
            }
        } else if (object instanceof ProcessingObject){
            DataType type = ((ProcessingObject) object).getDataType();
            if (type==DataType.SENSING_LOCATION){
                StorageManager.storeProcessedLocationDataLocally((ProcessingObject) object);

            }
        } else {
            throw new IncorrectDataTypeException(DataType.UNKNOWN, DataType.ANY);
        }
    }

    public static Object getDataFromStorage(DataType type, int amount){
        if (type==DataType.SENSING_BLUETOOTH){
            return BluetoothRepository.getInstance().getNumberOfTasks(amount);
        } else if (type==DataType.SENSING_LIGHTING) {
            return LightRepository.getInstance().getTasks();
        } else if (type==DataType.SENSING_LOCATION) {
            return LocationRepository.getInstance().getNumberOfTasks(amount);
        } else if (type==DataType.PROCESSING_LOCATION) {
            return ProcessedLocRepository.getInstance().getNumberOfTasks(amount);
        }
        return new ArrayList<>();
    }

    public static Object getDataFromStorage(DataType type){
        if (type==DataType.SENSING_BLUETOOTH){
            return BluetoothRepository.getInstance().getTasks();
        } else if (type==DataType.SENSING_LIGHTING) {
            return LightRepository.getInstance().getTasks();
        } else if (type==DataType.SENSING_LOCATION) {
            return LocationRepository.getInstance().getAllTasks();
        } else if (type==DataType.PROCESSING_LOCATION) {
            return ProcessedLocRepository.getInstance().getTasks();
        }
        return new ArrayList<>();
    }

    public static void deleteFromStorage(DataType type, int id){
        if (type==DataType.SENSING_BLUETOOTH){
            BluetoothRepository.getInstance().deleteTask(id);
        } else if (type==DataType.SENSING_LIGHTING) {
            LightRepository.getInstance().deleteTask(id);
        } else if (type==DataType.SENSING_LOCATION) {
            LocationRepository.getInstance().deleteTask(id);
        } else if (type==DataType.PROCESSING_LOCATION) {
            ProcessedLocRepository.getInstance().deleteTask(id);
        }
    }

    public static List<Object> getLocalData(DataType type, int nr_rows){
        return null;
    }

    /**
     * Stores the dataobject from LocationGpsSensingStrategy
     * @param dataobject
     * @throws IncorrectDataTypeException
     */
    public static void storeSensedLocationDataLocally(DataObject dataobject) throws IncorrectDataTypeException {

        if (dataobject.getDataType() != DataType.SENSING_LOCATION){
            throw new IncorrectDataTypeException(dataobject.getDataType(), DataType.SENSING_LOCATION);
        } else {
            SensingStrategyResolver.getRunningService(DataType.SENSING_LOCATION).insertDataObject(dataobject);
        }
    }

    public static void storeSensedBluetoothDataLocally(DataObject dataobject) throws IncorrectDataTypeException {

        if (dataobject.getDataType() != DataType.SENSING_BLUETOOTH){
            throw new IncorrectDataTypeException(dataobject.getDataType(), DataType.SENSING_BLUETOOTH);
        } else {
            SensingStrategyResolver.getRunningService(DataType.SENSING_BLUETOOTH).insertDataObject(dataobject);
        }
    }

    /**
     * Stores the processingobject from LocationProcessingStrategy
     * @param processingObject
     * @throws IncorrectDataTypeException
     */
    public static void storeProcessedLocationDataLocally(ProcessingObject processingObject) throws IncorrectDataTypeException{
        if (processingObject.getDataType() != DataType.SENSING_LOCATION){
            throw new IncorrectDataTypeException(processingObject.getDataType(), DataType.SENSING_LOCATION);
        } else {
            ProcessedLocRepository.getInstance().insertProcessingObject(processingObject);
        }
    }
}
