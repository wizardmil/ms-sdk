package seom.managers;

import seom.controller.DataType;
import seom.resolvers.ExportStrategyResolver;

public class ExportManager {

    private static ExportManager instance;

    private int K_ANONYMITY = 0;

    public ExportManager(){}

    /* The ExportManager is a singleton*/
    public static ExportManager getInstance(){
        if (instance == null){
            instance = new ExportManager();
        }
        return instance;
    }

    /** This method enables suppression by setting the K value to something above 0 */
    public ExportManager enableSuppression(int k_anonymity){
        this.K_ANONYMITY = k_anonymity;
        return ExportManager.getInstance();
    }

    /**
     * This the public that any export function goes through.
     * A client can call this method with a given data type
     * and the method will return a boolean:
     * true if success, false if fail.
     * @param dataType
     * @return
     */
    public void exportFromStorage(DataType dataType){
        if (dataType == DataType.SENSING_LOCATION) {
            ExportStrategyResolver.getInstance().exportLocationData();
        } else if (dataType == DataType.PROCESSING_LOCATION){
            ExportStrategyResolver.getInstance().exportClusterData();
        } else if (dataType == DataType.SENSING_BLUETOOTH) {
            ExportStrategyResolver.getInstance().exportBlueToothData();
        }
    }

    public boolean exportData(String fileName, String formattedData){
        return ExportStrategyResolver.getInstance().saveExternally(fileName, formattedData);
    }

}
