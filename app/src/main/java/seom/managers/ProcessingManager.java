package seom.managers;

import android.content.Context;
import android.util.Log;

import seom.controller.CONFIG;
import seom.resolvers.ProcessingStrategyResolver;
import seom.storage.IStrategyListener;

public class ProcessingManager {

    private int ms_samplingRate;
    /**
     * This manager has 2 constructors.
     * If the user has not initialized the Sensor Manager,
     * context has to be set and in that case,
     */
    public ProcessingManager(int ms_samplingRate){
        this.ms_samplingRate = ms_samplingRate;
    }

    public ProcessingManager(Context context, int ms_samplingRate){
        this.ms_samplingRate = ms_samplingRate;
        CONFIG.CONTEXT = context;
    }

    public ProcessingManager processLocationDataFromStorage(IStrategyListener storageStrategy, int batchSize, int k, int i){
        CONFIG.LOCATION_SAMPLINGRATE_PROCESSING = ms_samplingRate;
        CONFIG.LOCATION_BATCHSIZE_PROCESSING = batchSize;
        ProcessingStrategyResolver.processLocationData(CONFIG.MEMORY_ENABLED,storageStrategy, k, i);
        return this;
    }

    public ProcessingManager correlateLocationAndBlueTooth(int nr_blueTooth_devices_per_iteration, int nr_clusters_per_iteration){
        CONFIG.CORRELATION_SAMPLINGRATE_PROCESSING = ms_samplingRate + 1000;
        ProcessingStrategyResolver.startCorrelating(nr_blueTooth_devices_per_iteration, nr_clusters_per_iteration);
        return this;
    }

    public ProcessingManager enableSignalFiltering(){
        CONFIG.DO_SIGNAL_FILTERING = !CONFIG.DO_SIGNAL_FILTERING;
        return this;
    }

    public ProcessingManager disableMemory(){
        CONFIG.MEMORY_ENABLED = !CONFIG.MEMORY_ENABLED;
        return this;
    }

    public void cleanUP(){
        ProcessingStrategyResolver.cleanUp(CONFIG.CONTEXT);
    }
}
