package seom.managers;

import android.app.Activity;
import android.content.Context;

import androidx.core.app.ActivityCompat;

import seom.controller.CONFIG;
import seom.resolvers.SensingStrategyResolver;
import seom.storage.IStrategyListener;

public class SensingManager {

    private int ms_samplingRate = -1;

    public SensingManager(Context activity, int ms_samplingRate){
        CONFIG.CONTEXT = activity.getApplicationContext();
        CONFIG.ACTIVITY = activity;
        this.ms_samplingRate = ms_samplingRate;
    }

    public SensingManager(Context activity){
        CONFIG.CONTEXT = activity.getApplicationContext();
        CONFIG.ACTIVITY = activity;
    }

    /**
     * Makes any channel show itself as something else on lookup
     * e.g the location service shows itself as a timer
     * @return
     */
    public SensingManager flickObscure(){
        CONFIG.OBSCURE = !CONFIG.OBSCURE;
        return this;
    }

    /**
     * Gives the user quick access to requesting any permissions
     * @param permissions
     * @return
     */
    public SensingManager requestPermission(String[] permissions){
        ActivityCompat.requestPermissions((Activity) CONFIG.ACTIVITY, permissions, 1);
        return this;
    }

    /**
     * Before running this, make sure you have either:
     * ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION or INTERNET.
     * @param storingStrategy
     */
    public SensingManager initializeLocationSensing(IStrategyListener storingStrategy, int batchSize){
        if (ms_samplingRate>-1){
            CONFIG.LOCATION_BATCHSIZE_SENSING = batchSize;
            CONFIG.LOCATION_SAMPLINGRATE_SENSING = ms_samplingRate;
            SensingStrategyResolver.resolveLocationStrategy(storingStrategy);
        }
        return this;
    }

    public SensingManager initializeLocationSensing(IStrategyListener storingStrategy, int batchSize, int samplingRate){
        CONFIG.LOCATION_BATCHSIZE_SENSING = batchSize;
        CONFIG.LOCATION_SAMPLINGRATE_SENSING = samplingRate;
        SensingStrategyResolver.resolveLocationStrategy(storingStrategy);

        return this;
    }

    /**
     * Before running this, make sure you have either:
     * TODO: FIGURE PERMISSIONS FOR ILLUMINANCE
     * @param storingStrategy
     */
    public SensingManager initializeLightSensing(IStrategyListener storingStrategy, int batchSize){
        if (ms_samplingRate>-1){
            CONFIG.LIGHTING_BATCHSIZE_SENSING = batchSize;
            CONFIG.LIGHTING_SAMPLINGRATE_SENSING = ms_samplingRate;
            SensingStrategyResolver.resolveLightingStrategy(storingStrategy);
        }
        return this;
    }

    public SensingManager initializeLightSensing(IStrategyListener storingStrategy, int batchSize, int samplingRate){
        CONFIG.LIGHTING_BATCHSIZE_SENSING = batchSize;
        CONFIG.LIGHTING_SAMPLINGRATE_SENSING = samplingRate;
        SensingStrategyResolver.resolveLightingStrategy(storingStrategy);

        return this;
    }

    public SensingManager initializeBatterySensing(IStrategyListener storingStrategy){
        SensingStrategyResolver.resolveBatterySensingStrategy(storingStrategy);
        return this;
    }

    public SensingManager initializeBluetoothSensing(IStrategyListener storingStrategy, int batchSize){
        CONFIG.BLUETOOTH_BATCHSIZE_SENSING = batchSize;
        CONFIG.BLUETOOTH_SAMPLINGRATE_SENSING = 20000;
        SensingStrategyResolver.resolveBluetootheStrategy(storingStrategy);
        return this;
    }

    public void cleanUp(){
        SensingStrategyResolver.cleanUp(CONFIG.CONTEXT);
    }

}

