package seom.processing.Bluetooth;

import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import seom.controller.CONFIG;
import seom.controller.DataType;
import seom.exceptions.StrategyNotRunning;
import seom.internal.storage.room.databases.processing.ProcessedLocRepository;
import seom.internal.storage.room.databases.processing.correlation.CorrelationRepository;
import seom.internal.storage.room.databases.sensing.bluetooth.BluetoothRepository;
import seom.internal.storage.room.entities.BluetoothEntity;
import seom.internal.storage.room.entities.ClusterEntity;
import seom.internal.storage.room.entities.CorrelationEntity;
import seom.objects.ProcessingObject;
import seom.processing.AbstractProcessingStrategy;
import seom.processing.location.LocationProcessingStrategy;
import seom.resolvers.ProcessingStrategyResolver;

public class CorrelationProcessing extends AbstractProcessingStrategy {

    private Thread extrapolate;
    private List<BluetoothEntity> ble;
    private List<ClusterEntity> clusters;
    private long lastCalc = 0;
    private ProcessingObject data;
    private boolean running = false;

    public CorrelationProcessing(){
        super();
        data = new ProcessingObject(DataType.CORRELATE_BLUETOOTH_LOCATION);
        extrapolate = new Thread(() -> {
            running = true;
            while (true) {
                if (lastCalc + CONFIG.CORRELATION_SAMPLINGRATE_PROCESSING < System.currentTimeMillis()){
                    clusters = getClustersFromStorage();
                    ble = getBluetoothFromStorage();

                    analyze();

                    lastCalc = System.currentTimeMillis();
                }
            }
        });
    }

    private List<BluetoothEntity> getBluetoothFromStorage(){
        List<BluetoothEntity> ble_entities = BluetoothRepository.getInstance().getNumberOfTasks(K);
        return ble_entities;
    }

    private List<ClusterEntity> getClustersFromStorage(){
        List<ClusterEntity> storedEntities = ProcessedLocRepository.getInstance().getNumberOfTasks(I);
        return storedEntities;
    }

    private void analyze(){
        if (ble.size() > 0){
            List<CorrelationEntity> storedCor = CorrelationRepository.getInstance().getTasks();
            for (ClusterEntity p : clusters){

                List<BluetoothEntity> available = new ArrayList<>();
                Date start = null;
                Date end = null;
                try {
                    start = CONFIG.DATE_FORMAT.parse(p.getStartTime());
                    end = CONFIG.DATE_FORMAT.parse(p.getEndTime());
                } catch (ParseException e) {
                    continue;
                }

                for (BluetoothEntity b : ble){
                    Date btime = null;
                    try {
                        btime = CONFIG.DATE_FORMAT.parse(b.getDatetime());
                    } catch (ParseException e) {
                        continue;
                    }

                    if (start.getHours() <= btime.getHours() && end.getHours() >= btime.getHours()){
                        available.add(b);
                    }
                }
                if (available.size() > 0){

                    CorrelationEntity current_c = null;
                    for (CorrelationEntity c : storedCor){
                        if (c.getCluster().equals(p.toComparableString())){
                            current_c = c;
                        }
                    }

                    if (current_c != null) {
                        current_c.updateBlueToothEntities(available);
                        CorrelationRepository.getInstance().updateTask(current_c);
                        Log.d("DEBUGGING-PROCESSING", "updated " + clusters.size() + " correlating objects: {" + current_c.getBluetoothEntity() + ", " + current_c.getCluster()+"}");
                    } else {
                        CorrelationRepository.getInstance().insertTask(available, p);
                        Log.d("DEBUGGING-PROCESSING", "stored " + clusters.size() + " correlating objects: {" + available.toString() + ", " + p.toString()+"}");
                    }
                }
            }

            CorrelationRepository.getInstance().delteButKeep(50);
        }
    }

    @Override
    public void process() {
        if (ProcessingStrategyResolver.validateProcessingStarted(DataType.SENSING_LOCATION) && !running){
            extrapolate.start();
        } else {
            try {
                throw new StrategyNotRunning(LocationProcessingStrategy.class, this.getClass());
            } catch (StrategyNotRunning strategyNotRunning) {
                // do nothing
            }
        }
    }

    @Override
    public ProcessingObject getProcessingObject() {
        return data;
    }

    @Override
    public void reset() {

    }
}
