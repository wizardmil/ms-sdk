package seom.processing;

import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;

import seom.controller.ProcessRestarter;
import seom.controller.ServiceStarter;
import seom.objects.DataObject;
import seom.resolvers.ProcessingStrategyResolver;
import seom.storage.IStrategyListener;

public abstract class AbstractProcessingStrategy extends Service implements IProcessingStrategy {

    public IStrategyListener STORING_STRATEGY;
    public String NOTIFICATION_CHANNEL_ID = null, CHANNEL_NAME = null;

    public boolean FROM_MEMORY = false, FROM_STORAGE = false;
    public DataObject DATAOBJECT = null;
    public int K, I;

    public AbstractProcessingStrategy(){ }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (CHANNEL_NAME == null && NOTIFICATION_CHANNEL_ID == null){

            Bundle serviceParams = intent.getExtras();
            NOTIFICATION_CHANNEL_ID = (String) serviceParams.get("NOTIFICATION_CHANNEL_ID");
            CHANNEL_NAME = (String) serviceParams.get("CHANNEL_NAME");

            try {
                STORING_STRATEGY = (IStrategyListener) serviceParams.get("STORING_STRATEGY");
            } catch(Exception e){}

            try {
                K = (int) serviceParams.get("K");
                I = (int) serviceParams.get("I");
            } catch(Exception e){}

            try {
                FROM_MEMORY = (boolean) serviceParams.get("FROM_MEMORY");
                FROM_STORAGE = (boolean) serviceParams.get("FROM_STORAGE");
            } catch(Exception e){}

            try {
                DATAOBJECT = (DataObject) serviceParams.get("DATAOBJECT"); // TODO: Not currently used, consider what to do with it
            } catch(Exception e){}

            ServiceStarter.initService(NOTIFICATION_CHANNEL_ID, CHANNEL_NAME, this, Color.GREEN, "processing.strategy."+CHANNEL_NAME);

            process();
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        ProcessingStrategyResolver.appendToRestart(this.getClass());

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartprocess");

        broadcastIntent.setClass(this, ProcessRestarter.class);
        this.sendBroadcast(broadcastIntent);
    }
}
