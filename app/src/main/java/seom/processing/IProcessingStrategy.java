package seom.processing;

import seom.objects.ProcessingObject;

public interface IProcessingStrategy {

    void process();
    ProcessingObject getProcessingObject();
    void reset();
}
