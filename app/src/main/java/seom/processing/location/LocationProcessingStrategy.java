package seom.processing.location;

import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.util.Pair;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import seom.controller.CONFIG;
import seom.controller.DataType;
import seom.internal.storage.room.databases.sensing.location.LocationRepository;
import seom.internal.storage.room.entities.LocationEntity;
import seom.objects.PointDoubleLocation;
import seom.objects.KMeansCollection;
import seom.objects.ProcessingObject;
import seom.processing.AbstractProcessingStrategy;
import seom.resolvers.SensingStrategyResolver;


public class LocationProcessingStrategy extends AbstractProcessingStrategy {

    private Thread extrapolate;
    private List<Pair<String, String>> currentData;
    private long lastCalc = 0;
    private Geocoder geocoder;
    private List<Address> addresses;
    public ProcessingObject data;
    private boolean running = false;

    public LocationProcessingStrategy(){
        super();

        data = new ProcessingObject(DataType.SENSING_LOCATION);

        geocoder = new Geocoder(CONFIG.CONTEXT, Locale.getDefault());

        extrapolate = new Thread(() -> {
            running = true;
                while (true) {

                    if (lastCalc + CONFIG.LOCATION_SAMPLINGRATE_PROCESSING < System.currentTimeMillis()){

                        if (FROM_MEMORY){
                            currentData = SensingStrategyResolver.getDataObjectFromServiceMemory(DataType.SENSING_LOCATION, CONFIG.LOCATION_BATCHSIZE_PROCESSING);
                        } else if (FROM_STORAGE){
                            currentData = getLocationFromStorage();
                        }

                        if (currentData != null) {
                            if (currentData.size() >= CONFIG.LOCATION_BATCHSIZE_PROCESSING){
                                if (CONFIG.DO_SIGNAL_FILTERING){
                                    analyze(filterSignal());
                                } else {
                                    analyze(formatCurrentData());
                                }
                            }
                        }
                        lastCalc = System.currentTimeMillis();
                    }

            }
        });
    }

    private List<Pair<String, String>> getLocationFromStorage(){
        // we only want to grab the newest batch of location coordinates
        List<LocationEntity> storedEntities = LocationRepository.getInstance().getNumberOfTasks(CONFIG.LOCATION_BATCHSIZE_PROCESSING);
        Log.d("DEBUGGING-PROCESSING", "grabbed "+ storedEntities.size() + " entities from db");
        List<Pair<String, String>> data = new ArrayList<>();
        for (LocationEntity e : storedEntities){
            data.add(e.getAsPair());
        }
        return data;
    }

    /**
     * Formats the raw data list into a KMeanCollection object
     * @return
     */
    private KMeansCollection formatCurrentData(){
        KMeansCollection coords = new KMeansCollection();

        for(int i = 0; i < currentData.size(); i++){
            coords.add(
                     new PointDoubleLocation(Double.parseDouble(currentData.get(i).first.split(",")[0]), Double.parseDouble(currentData.get(i).first.split(",")[1]), currentData.get(i).second, currentData.get(i).second)
            );
        }

        return coords;
    }

    @Override
    public ProcessingObject getProcessingObject() {
        return data;
    }

    /**
     * Calucates the mean of the x's and the y's and creates a new Point Double Location on them.
     * Adds new newly created point to the parametized list and returns.
     * @param coords
     * @param x
     * @param y
     * @return
     */
    private KMeansCollection addNewMeanPoint(KMeansCollection coords, List<Double> x, List<Double> y, List<String> start, List<String> end){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        double sx = 0;
        double sy = 0;
        long ss = 0;
        long es = 0;
        for(Double d : x){
            sx += d;
        }
        for(Double d : y){
            sy += d;
        }
        for(String d : start){
            try {
            ss += format.parse(d).getTime();

            }catch (ParseException e) {
                e.printStackTrace();
            }
        }
        for(String d : end){
            try {
                es += format.parse(d).getTime();

            }catch (ParseException e) {
                e.printStackTrace();
            }
        }

        /* Sometimes results in Double.NAN, so this is a fail/safe */
        if (x.size() > 0 && y.size() > 0){
            double xPosition = sx / x.size();
            double yPosition = sy / y.size();
            long ns = ss / start.size();
            long ne = es / end.size();
            coords.add(new PointDoubleLocation(xPosition, yPosition, format.format(new Date(ns)), format.format(new Date(ne))));
        }

        return coords;
    }

    /**
     * Performs MEAN-FILTERING on the signal to reduce outliers and
     * makes the measured trajectory more like the true trajectory
     * @return
     */
    private KMeansCollection filterSignal(){
        KMeansCollection coordinates = formatCurrentData();
        KMeansCollection newCoordinates = new KMeansCollection();
        List<Double> x = new ArrayList<>();
        List<Double> y = new ArrayList<>();
        List<String> s = new ArrayList<>();
        List<String> e = new ArrayList<>();
        for (int i = 0; i < coordinates.size(); i++){
            if (i % 5 == 0){
                newCoordinates = addNewMeanPoint(newCoordinates, x, y, s, e);
                x = new ArrayList<>();
                y = new ArrayList<>();
                s = new ArrayList<>();
                e = new ArrayList<>();
            } else {
                PointDoubleLocation point = coordinates.get(i);
                x.add(point.getX());
                y.add(point.getY());
                s.add(point.getStartTime());
                e.add(point.getEndTime());
            }
        }

        if (x.size() > 0 && y.size() > 0){
            newCoordinates = addNewMeanPoint(newCoordinates, x, y, s, e);
        }
        return newCoordinates;
    }

    /**
     * Analyses the processed trajectory my performin K-MEANS clustering
     * and does a lookup in the geocoder, to find address.
     * @param inputCoordinates
     */
    private void analyze(KMeansCollection inputCoordinates){
        try {
            HashMap<String, PointDoubleLocation> clusterResults = inputCoordinates.calculateKMeans(K, I);
            for(String key : clusterResults.keySet()){

                addresses = geocoder.getFromLocation(clusterResults.get(key).getX(), clusterResults.get(key).getY(), 3); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                for (int i = 0; i < addresses.size(); i ++ ){

                    HashMap<String, ArrayList<String>> place = new HashMap<>();
                    ArrayList<String> information = new ArrayList<>();

                    information.add(addresses.get(i).getAddressLine(0)); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    information.add(addresses.get(i).getLocality());
                    information.add(addresses.get(i).getAdminArea());
                    information.add(addresses.get(i).getCountryName());
                    information.add(addresses.get(i).getPostalCode());
                    information.add(addresses.get(i).getFeatureName());
                    information.add(clusterResults.get(key).getStartTime());
                    information.add(clusterResults.get(key).getEndTime());

                    place.put(key, information);

                    data.addData(place);
                }
            }

            storeData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void storeData(){

        if (STORING_STRATEGY != null){
            STORING_STRATEGY.onResponseProcessingObject(data);
        }

        data.reset();
    }

    @Override
    public void process() {
        if (!running)
            extrapolate.start();
    }

    @Override
    public void reset() {
        data = new ProcessingObject(DataType.SENSING_LOCATION);
    }
}
