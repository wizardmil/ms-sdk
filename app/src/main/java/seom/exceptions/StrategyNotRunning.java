package seom.exceptions;

public class StrategyNotRunning extends Exception {

    public StrategyNotRunning(Class shouldRun, Class current){
        super(String.format("%s, must run in order for %s to function.", shouldRun.toString(), current.toString()));
    }
}
