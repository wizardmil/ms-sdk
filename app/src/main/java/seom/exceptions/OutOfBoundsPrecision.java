package seom.exceptions;

public class OutOfBoundsPrecision extends Exception {

    public OutOfBoundsPrecision(){
        super("The precision variable, p, must be 1<=p<=10.");
    }
}