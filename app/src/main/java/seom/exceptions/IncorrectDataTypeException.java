package seom.exceptions;

import seom.controller.DataType;

public class IncorrectDataTypeException extends Exception {

    public IncorrectDataTypeException(DataType currentType, DataType shouldbe){
        super(String.format("The data type of this object must be %s. This type is %s.", shouldbe.toString(), currentType.toString()));
    }
}
